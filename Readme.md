# Correspondance de Proust

Version complètement réécrite, et les différentes dépendances mises à jour
- Symfony 5
- Saxon
- PHP 7

## Installation
- copier le .env.dist en .env
- copier le ./application/.env.dist en ./application/.env
- `docker-compose build`
- `docker-compose up -d`
- `docker-compose exec apache bash`
  - `make update`
  - créer un compte puis :
  - `php bin/console app:set-role <mail> ROLE_SUPER_ADMIN`

## Mise à jour
- git pull
- `docker-compose exec apache bash`
  - `make update`
  - ou bien `npm run dev` peut suffire


## Commandes

* passer un compte en admin :
`php bin/console app:set-role <mail> ROLE_SUPER_ADMIN`

* supprimer toutes les lettres
`php bin/console app:delete-letters`

* import letters
copy xml files into `application/import` && `php bin/console app:import`

* supprimer le cache pour une lettre
`php bin/console app:cache:letter:invalidate <letterId>`

* supprimer le cache pour toutes les lettres
`php bin/console app:cache:invalidate `

* Préparer le cache pour toutes les lettres
`php bin/console app:cache:warmup `

## Basculement entre instance de prod et de preprod
On passe d'une instance de prod à préprod en changeant la valeur d'`INSTANCE` dans `application/.env`. Ajouter la ligne si besoin, en prenant exemple sur  `application/.env.dist` (dans le cas ou l'installation de l'instance est antérieur au développement de cette fonctionnalité)

## Fonctionalité de recherche
Une fois le site déployé, **via le sous-menu "Rechercher" du menu "Lettres", il est possible d'accéder à la fonctionnalité de recherche**.
- **Dans quoi cherche-t-on ?** Par défaut, la chaîne de caractères entrée est confrontée au contenu des lettres, mais il est possible aussi de chercher dans les notes et métadonnées via une case à cocher.
- **Que cherche-t-on ?**
  - **Il est possible de chercher n'importe quel mot**, par exemple `ami`Par défaut, la recherche est insensible à la casse : on obtiendra donc des lettres contenant "ami" ou "Ami".
  - **Il est possible de rendre sa recherche sensible à la casse** via une case à cocher. Ainsi, la recherche `Ami` ne renverra que le lettres avec une majuscule en début de mot.
  - **Il est possible de chercher un portion de mot**. Si l'on souhaite une recherche plus souple, par exemple trouver à la fois "ami" et "amis", "amies", "amitié", etc., il est possible d'utiliser le joker "*" : `ami*`. Celui-ci peut de la même façon être utiliser en début de mot : `*ami` renverra autant des lettres contenant "ami" que des lettres contenant "Aramis".
- **Comment s'affichent les résultats ?** La liste des résultats est affichée dans la même interface que celle utilisée pour toutes les lettres (c'est à dire qu'on peut refiltrer derrière par date, expéditeur, destinataire, etc.).<br/>
Pour l'instant, il n'y a pas de focus ou de mise en exergue de la chaîne qui aurait matché à l'affichage de la lettre.

## Systèmes de cache
**Un système de cache des sorties XSLT** a été mis en place. Il permet de sauvegarder les sorties des transformations XSLT. Initialement développé pour ne pas avoir à recalculer les sorties XSLT à chaque recherche, ce système bénéficie au passage à l'affichage des lettre dont le contenu n'est plus recalculé systématiquement et dont l'affichage est ainsi sensiblement plus rapide.

Le cache peut être recalculé manuellement : voir la section "Commandes" ci-dessus"

** Il existe aussi un système de cache dit UIUC** pour les informations biographiques liées au noms de personne. Nous recupérons en effet des informations depuis le Kolb-Proust Archive for Research: Names Database (https://kolbproust-nd.library.illinois.edu/). Afin d'alléger les flux de données et de ne pas être dépendant d'un site externe pour des quantité de données faibles (en terme d'espace disque informatique), nous faisons une copie locale des informations qui nous sont utiles.

Ce cache peut être recalculé de deux façons :
  * Directement sur le site déployé, en étant connecté avec un compte admin, sur la page $url/cache/list ou bien 
  * Depuis le serveur : voir la section "Commandes" ci-dessus"

