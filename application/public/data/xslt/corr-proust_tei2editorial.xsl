<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE corr-proust_tei2html_ask-me_version [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:sf="http://saxon.sf.net/" xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xs tei sf" version="2.0">

    <xsl:param name="slug">acknowledgments</xsl:param>
    <xsl:param name="lang">fr</xsl:param>
    <xsl:param name="xml-dir">../../upload/edito/</xsl:param>

    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/">
        <xsl:variable name="collection" select="concat($xml-dir, '?select=*.xml')"/>
        <xsl:variable name="tei_file" select="collection($collection)"/>
        <xsl:variable name="xml_id" select="concat($slug, '_', $lang)"/>
        <div class="col">
            <xsl:choose>
                <xsl:when test="count($tei_file//tei:text[@xml:id = $xml_id]) = 1">
                    <xsl:apply-templates select="$tei_file//tei:text[@xml:id = $xml_id]"/>
                </xsl:when>
                <xsl:otherwise>
                    <p>
                        <i>Page en construction</i>
                    </p>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>

    <xsl:template match="tei:text">
        <xsl:apply-templates
            select="../tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@xml:lang = $lang]"/>
        <xsl:if test="name(tei:body/child::*[1]) = 'p'">
            <p class="chapeau">
                <xsl:apply-templates select="tei:body/child::*[1]" mode="inside"/>
            </p>
        </xsl:if>
        <xsl:apply-templates select="tei:body/child::*[position() > 1]"/>
    </xsl:template>

    <xsl:template match="tei:div[@synch]">
        <xsl:variable name="synch" select="@synch"/>
        <xsl:apply-templates select="/tei:TEI//tei:*[concat('#', @xml:id) = $synch]"/>
    </xsl:template>
    <xsl:template match="tei:div">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:p">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:p" mode="inside">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:lb">
        <br/>
    </xsl:template>
    <xsl:template match="tei:title">
        <h1>
            <xsl:apply-templates/>
        </h1>
    </xsl:template>
    <xsl:template match="tei:head">
        <xsl:element name="h3">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'italic']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:ref">
        <a href="{@target}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    <xsl:template match="tei:list">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:figure">
        <img alt="{tei:figDesc}" src="{tei:graphic/@url}"/>
    </xsl:template>
    <xsl:template match="tei:email">
        <mark>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>
    <xsl:template match="*">
        <b style="color:red">
            <xsl:value-of select="name(.)"/>
        </b>
    </xsl:template>
</xsl:stylesheet>
