<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE corr-proust_tei2html_ask-me_version [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:sf="http://saxon.sf.net/" xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xs tei sf" version="2.0">
    <!-- See manual: https://wiki.illinois.edu/wiki/pages/viewpage.action?pageId=728159945#Corr-Proust:Manueldetranscription(outilv8)-4.2.Normestypographiquespourlestranscriptions -->

    <xsl:param name="query">diplo</xsl:param>
    <xsl:param name="search_word">chambre</xsl:param>
    <xsl:param name="idletter">02969</xsl:param>
    <xsl:param name="xml-dir">../../upload/letters/</xsl:param>
    <xsl:param name="list_institutions_file"
        >../../upload/common/CP_liste_institutions.xml</xsl:param>
    <xsl:param name="list_institutions" as="node()*">
        <xsl:if test="doc-available($list_institutions_file)">
            <xsl:variable name="institutions_file"
                select="collection(concat($xml-dir, '../common/.?select=*_*.xml'))"/>
            <xsl:copy-of select="$institutions_file"/>
        </xsl:if>
    </xsl:param>
    <!-- Useful only for standalone mode -->
    <xsl:param name="thumbnail-height">80</xsl:param>
    <xsl:param name="image-width">1600</xsl:param>

    <xsl:param name="standalone">0</xsl:param>
    <xsl:param name="standalone_mode_to_apply">
        <xsl:text>title|facs|diplo|linear|note|metadata|json</xsl:text>
        <!--<xsl:text>facs</xsl:text>-->
    </xsl:param>
    <xsl:param name="debug">0</xsl:param>
    <!-- END - Useful only for standalone mode -->


    <xsl:output method="html" indent="no" omit-xml-declaration="yes"/>
    <!--<xsl:strip-space elements="*"/>
    <xsl:strip-space elements="tei:*"/>
    <xsl:preserve-space elements="tei:placeName"/>-->

    <xsl:template match="/">
        <xsl:variable name="tei_file"
            select="collection(concat('../../upload/letters/?select=', $idletter, '*.xml'))"/>
        <xsl:variable name="tei_testfile"
            select="collection(concat('../../upload/letters/?select=', 'test.xml'))"/>
        <xsl:choose>
            <xsl:when test="$standalone = 1">
                <!-- Template qui s'applique à la racine du XML -->
                <html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
                    <head>
                        <title>
                            <!-- Ici, on va chercher explicitement la valeur d'un élément spécifique -->
                            <xsl:value-of
                                select="$idletter/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"
                            />
                        </title>
                        <link
                            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                            rel="stylesheet"
                            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                            crossorigin="anonymous"/>
                        <!-- Bootstrap extension pour rotation de texte - https://bootstrap-extension.com/index.html -->
                        <!--<link rel="stylesheet" href="//bootstrap-extension.com/bootstrap-extension-5.1.0/css/bootstrap-extension.min.css" type="text/css"/>
                    <script src="//bootstrap-extension.com/bootstrap-extension-5.1.0/js/bootstrap-extension.min.js"></script>-->
                        <!-- End Bootstrap extension -->
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"/>
                        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"/>
                        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"/>
                        <style>
                            h1,
                            h2 {
                                text-align: center;
                                padding: 1em;
                            }
                            .gris {
                                background-color: gray;
                            }
                            .bleu {
                                background-color: blue;
                            }
                            .btn.metadata {
                                border-color: #0d6efd;
                            }
                            .btn.diplo {
                                border-color: #fd7e14;
                            }
                            .btn.linear {
                                border-color: #198754;
                            }
                            .btn.title {
                                border-color: #0dcaf0;
                            }
                            .btn.note {
                                border-color: #6f42c1;
                            }</style>
                    </head>
                    <body style="margin-bottom: 15px;">
                        <h1> Mode standalone Corr-Proust - Letter <xsl:value-of select="$idletter"
                            /></h1>
                        <div class="container">
                            <pre><xsl:value-of select="concat('../../upload/letters/?select=', $idletter, '*.xml')"/></pre>
                            <xsl:for-each select="tokenize($standalone_mode_to_apply, '\|')">
                                <div id="{.}">
                                    <h2>Mode <xsl:value-of select="."/></h2>
                                    <xsl:choose>
                                        <xsl:when test=". = 'json' or . = 'json_one'">
                                            <pre><xsl:apply-templates select="$tei_testfile[1]/tei:TEI">
                                            <xsl:with-param name="query" select="."/>
                                        </xsl:apply-templates></pre>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:apply-templates select="$tei_file[1]/tei:TEI">
                                                <xsl:with-param name="query" select="."/>
                                            </xsl:apply-templates>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </div>
                            </xsl:for-each>
                        </div>
                        <xsl:if test="$debug = 1">
                            <h2>Mode debug</h2>
                            <xsl:call-template name="hard_debug"/>
                        </xsl:if>
                    </body>
                </html>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates>
                    <xsl:with-param name="query" select="$query"/>
                </xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:TEI" mode="#all">
        <xsl:param name="query"/>
        <xsl:choose>
            <xsl:when test="$query = 'title'">
                <xsl:apply-templates mode="title"
                    select="tei:teiHeader/tei:profileDesc/tei:correspDesc"/>
            </xsl:when>
            <xsl:when test="$query = 'facs'">
                <xsl:apply-templates mode="facs" select="tei:text//tei:pb"/>
            </xsl:when>
            <xsl:when test="$query = 'diplo'">
                <xsl:apply-templates mode="diplo" select="tei:text/tei:body"/>
            </xsl:when>
            <xsl:when test="$query = 'linear'">
                <xsl:apply-templates mode="linear" select="tei:text/tei:body"/>
            </xsl:when>
            <xsl:when test="$query = 'note'">
                <xsl:apply-templates mode="note" select="tei:text"/>
            </xsl:when>
            <xsl:when test="$query = 'metadata'">
                <xsl:apply-templates mode="metadata" select="tei:teiHeader"/>
            </xsl:when>
            <xsl:when test="$query = 'json'">
                <xsl:apply-templates mode="json" select="tei:teiHeader"/>
            </xsl:when>
            <xsl:when test="$query = 'json_one'">
                <xsl:apply-templates mode="json_one" select="tei:teiHeader"/>
            </xsl:when>
            <xsl:when test="$query = 'json_search'">
                <xsl:apply-templates mode="json_search" select="tei:text/tei:body"/>
            </xsl:when>
            <xsl:when test="$query = 'debug-info'">
                <xsl:apply-templates mode="debug-info"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message>Unknown query <xsl:value-of select="$query"/></xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- QUERY 'debug' -->
    <xsl:template match="tei:TEI" mode="debug-info">
        <!--<div class="debug-info">Pour l'exemple</div>-->
    </xsl:template>

    <!-- QUERY 'title'-->
    <xsl:template match="tei:correspDesc" mode="title">
        <span id="cp">
            <xsl:text>CP </xsl:text>
            <xsl:value-of select="substring-after(@xml:id, 'cp')"/>
        </span>
        <xsl:text> </xsl:text>
        <span id="from-name">
            <xsl:apply-templates select="tei:correspAction[@type = 'write']/tei:persName"
                mode="#current"/>
        </span>
        <xsl:text> à </xsl:text>
        <span id="to-name">
            <xsl:apply-templates select="tei:correspAction[@type = 'received']/tei:persName"
                mode="#current"/>
        </span>
        <xsl:text> </xsl:text>
        <span id="date">
            <xsl:value-of select="tei:correspAction[@type = 'write']/tei:date"/>
            <xsl:if
                test="not(tei:correspAction[@type = 'write']/tei:date[substring(., 1, 1) = '[' and substring(., string-length(.), 1) = ']'])">
                <xsl:if test="$debug = 1">
                    <xsl:message>[cp<xsl:value-of select="$idletter"/>] Date mal encodée. "[" et "]"
                        attendus en début et fin de chaîne. </xsl:message>
                </xsl:if>
            </xsl:if>
        </span>
    </xsl:template>
    <xsl:template match="tei:persName" mode="title json">
        <xsl:choose>
            <xsl:when test="tei:addName">
                <xsl:apply-templates select="tei:addName" mode="#current"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space(tei:forename)"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="normalize-space(tei:surname)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:addName" mode="title json">
        <xsl:choose>
            <xsl:when test="tei:forename and tei:surname">
                <xsl:value-of select="normalize-space(tei:forename)"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="normalize-space(tei:surname)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space(.)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- QUERY 'facs'-->
    <xsl:template match="tei:pb" mode="facs">
        <xsl:variable name="pb_num">
            <xsl:number count="//tei:pb" level="any"/>
        </xsl:variable>
        <img class="img-thumbnail">
            <xsl:attribute name="id">
                <xsl:text>image-{{pos}}-</xsl:text>
                <xsl:value-of select="$pb_num - 1"/>
            </xsl:attribute>
            <xsl:attribute name="data-pos">
                <xsl:text>{{pos}}</xsl:text>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test="@facs = 'photocopie.jpg'">
                    <xsl:attribute name="src">
                        <xsl:text>/img/facs/non_communicable.png</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="data-img">
                        <xsl:text>/img/facs/non_communicable.png</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="data-info">
                        <xsl:text>na</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="@facs = 'pas_image.jpg'">
                    <xsl:attribute name="src">
                        <xsl:text>/img/facs/non_disponible.png</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="data-info">
                        <xsl:text>na</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="data-img">
                        <xsl:text>/img/facs/non_disponible.png</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="starts-with(@facs, 'http')">
                    <xsl:attribute name="src">
                        <xsl:value-of select="@facs"/>
                    </xsl:attribute>
                    <xsl:attribute name="data-img">
                        <xsl:value-of select="@facs"/>
                    </xsl:attribute>
                    <xsl:attribute name="data-credit">
                        <xsl:variable name="institution"
                            select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:institution/@corresp"/>
                        <xsl:if test="
                                not($list_institutions = '')
                                and not($institution = '')
                                and count($list_institutions/tei:TEI/tei:standOff/tei:listOrg/tei:org[concat('#', @xml:id) = $institution]/tei:note[@type = 'credits']) = 1">
                            <xsl:value-of
                                select="$list_institutions/tei:TEI/tei:standOff/tei:listOrg/tei:org[concat('#', @xml:id) = $institution]/tei:note[@type = 'credits']"
                            />
                        </xsl:if>
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when
                            test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:institution/@corresp = '#UIUC'">
                            <xsl:attribute name="src">
                                <xsl:text>https://images.digital.library.illinois.edu/iiif/2/</xsl:text>
                                <xsl:value-of select="@facs"/>
                                <xsl:text>/full/,</xsl:text>
                                <xsl:value-of select="$thumbnail-height"/>
                                <xsl:text>/0/color.jpg</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="data-img">
                                <xsl:text>https://images.digital.library.illinois.edu/iiif/2/</xsl:text>
                                <xsl:value-of select="@facs"/>
                                <xsl:text>/full/</xsl:text>
                                <xsl:value-of select="$image-width"/>
                                <xsl:text>,/0/color.jpg</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="data-credit">
                                <xsl:variable name="institution"
                                    select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:institution/@corresp"/>
                                <xsl:if test="
                                        not($list_institutions = '')
                                        and not($institution = '')
                                        and count($list_institutions/tei:TEI/tei:standOff/tei:listOrg/tei:org[concat('#', @xml:id) = $institution]/tei:note[@type = 'credits']) = 1">
                                    <xsl:value-of
                                        select="$list_institutions/tei:TEI/tei:standOff/tei:listOrg/tei:org[concat('#', @xml:id) = $institution]/tei:note[@type = 'credits']"
                                    />
                                </xsl:if>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="$debug = 1">
                                <xsl:message>
                                    <!--<xsl:text>[</xsl:text>
                                    <xsl:value-of select="sf:current-mode-name()"/>
                                    <xsl:text>] </xsl:text>-->
                                    <xsl:text>@facs ne commençant pas par "http" et institution autre que "#UIUC". </xsl:text>
                                    <xsl:copy/>
                                    <xsl:copy-of select="@*"/>
                                </xsl:message>
                            </xsl:if>
                            <xsl:attribute name="src">
                                <xsl:text>/img/facs/non_disponible.png</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="data-info">
                                <xsl:text>na</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="data-img">
                                <xsl:text>/img/facs/non_disponible.png</xsl:text>
                            </xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </img>
    </xsl:template>

    <!-- QUERY 'diplo' and 'linear'-->
    <xsl:template match="tei:body" mode="diplo linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:div[@type = 'letter']" mode="diplo">
        <xsl:variable name="font-type">
            <xsl:choose>
                <xsl:when
                    test="contains(/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/@form, 'allographe')">
                    <xsl:text>manuscrit</xsl:text>
                </xsl:when>
                <xsl:when
                    test="contains(/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/@form, 'autographe')">
                    <xsl:text>manuscrit</xsl:text>
                </xsl:when>
                <xsl:when
                    test="contains(/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/@form, 'dactylo_carbone')">
                    <xsl:text>dactylo</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>manuscrit</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when
                    test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:handDesc/tei:handNote/@medium = 'encre_bleue'">
                    <xsl:text> c-bleu</xsl:text>
                </xsl:when>
                <xsl:when
                    test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:handDesc/tei:handNote/@medium = 'encre_rouge'">
                    <xsl:text> c-rouge</xsl:text>
                </xsl:when>
                <xsl:when
                    test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:handDesc/tei:handNote/@medium = 'encre_violette'">
                    <xsl:text> c-violet</xsl:text>
                </xsl:when>
                <xsl:when
                    test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:handDesc/tei:handNote/@medium = 'encre_verte'">
                    <xsl:text> c-vert</xsl:text>
                </xsl:when>
                <xsl:when
                    test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:handDesc/tei:handNote/@medium = 'crayon_graphique'">
                    <xsl:text> c-gris</xsl:text>
                </xsl:when>
                <!-- crayon_couleur, mixte, encre_noire : RAS -->
            </xsl:choose>
        </xsl:variable>
        <div>
            <xsl:choose>
                <xsl:when
                    test="//tei:add[@place = 'margin_left'] and //tei:add[@place = 'margin_right']">
                    <xsl:attribute name="class">
                        <xsl:text>offset-1 col-10 </xsl:text>
                        <xsl:value-of select="$font-type"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="//tei:add[@place = 'margin_left']">
                    <xsl:attribute name="class">
                        <xsl:text>offset-1 col-11 </xsl:text>
                        <xsl:value-of select="$font-type"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="//tei:add[@place = 'margin_right']">
                    <xsl:attribute name="class">
                        <xsl:text>col-11 </xsl:text>
                        <xsl:value-of select="$font-type"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class">
                        <xsl:text>col-12 </xsl:text>
                        <xsl:value-of select="$font-type"/>
                    </xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="//*[@place = 'top_letter']" mode="diplo_top_letter"/>
            <xsl:apply-templates mode="#current"/>
            <xsl:apply-templates select="//*[@place = 'bottom_letter']" mode="diplo_bottom_letter"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:div[@type = 'letter']" mode="linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:pb[@facs]" mode="diplo">
        <xsl:variable name="last_pb_id">
            <xsl:value-of select="preceding::tei:pb[1]/generate-id()"/>
        </xsl:variable>

        <xsl:apply-templates
            select="//*[@place = 'bottom_page' and preceding::tei:pb[1]/generate-id() = $last_pb_id]"
            mode="diplo_bottom_page"/>
        <xsl:apply-templates
            select="//*[@place = 'bottom_right' and preceding::tei:pb[1]/generate-id() = $last_pb_id]"
            mode="diplo_bottom_page"/>
        <xsl:variable name="pb_num">
            <xsl:number count="//tei:pb" level="any"/>
        </xsl:variable>
        <hr>
            <xsl:attribute name="data-img-id">
                <xsl:value-of select="$pb_num - 1"/>
            </xsl:attribute>
        </hr>
        <xsl:if test="name(following-sibling::*[1]) = 'pb'">
            <xsl:text>&non_breakable_space;</xsl:text>
        </xsl:if>
        <!-- TODO : à vérifier -->
        <xsl:variable name="current_pb_id">
            <xsl:value-of select="preceding::tei:pb[1]/generate-id()"/>
        </xsl:variable>
        <xsl:apply-templates
            select="//*[@place = 'top_page' and preceding::tei:pb[1]/generate-id() = $last_pb_id]"
            mode="diplo_top_page"/>
    </xsl:template>
    <xsl:template match="tei:pb[@facs]" mode="linear">
        <xsl:variable name="pb_num">
            <xsl:number count="//tei:pb" level="any"/>
        </xsl:variable>
        <span class="page-break">
            <xsl:attribute name="data-img-id">
                <xsl:value-of select="$pb_num - 1"/>
            </xsl:attribute>
        </span>
        <!-- <xsl:text>&non_breakable_space;</xsl:text> -->
        <xsl:if test="name(following-sibling::*[1]) = 'pb'">
            <xsl:text>&non_breakable_space;</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:opener" mode="linear">
        <div class="opener">
            <xsl:choose>
                <xsl:when test="@place = 'center'">
                    <xsl:attribute name="class">
                        <xsl:text>opener text-center</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="@place = 'left'">
                    <xsl:attribute name="class">
                        <xsl:text>opener text-start</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="@place = 'right'">
                    <xsl:attribute name="class">
                        <xsl:text>opener text-end</xsl:text>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:opener[not(@place)]" mode="diplo">
        <div class="opener">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>opener droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:opener[@place = 'bottom_letter']" mode="diplo">
        <div class="opener">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>opener droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:opener[@place = 'bottom_page']" mode="diplo">
        <div class="opener">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>opener droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:opener[@place = 'center']" mode="diplo">
        <div class="opener text-center">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>opener text-center droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:opener[@place = 'left']" mode="diplo">
        <div class="opener text-start">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>opener text-start droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:opener[@place = 'right']" mode="diplo">
        <div class="opener text-end">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>opener text-end droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:closer" mode="linear">
        <div class="closer">
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:closer[not(@place)]" mode="diplo linear">
        <div class="closer">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>closer droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:closer[@place = 'top_letter']" mode="diplo_top_letter">
        <div class="closer">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>closer droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:closer[@place = 'top_page']" mode="diplo_top_page">
        <div class="closer">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>closer droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:closer[@place = 'center']" mode="diplo">
        <div class="closer text-center">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>closer text-center droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:closer[@place = 'left']" mode="diplo">
        <div class="closer text-start">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>closer text-start droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:closer[@place = 'right']" mode="diplo">
        <div class="closer text-end">
            <xsl:if test="@type = 'en_tête'">
                <xsl:attribute name="class">
                    <xsl:text>closer text-end droit printed</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:postscript" mode="linear">
        <div class="postscriptum">
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:postscript[not(@place)]" mode="diplo">
        <div class="postscriptum">
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:postscript[@place = 'top_letter']" mode="diplo"/>
    <xsl:template match="tei:postscript[@place = 'top_page']" mode="diplo"/>
    <xsl:template match="tei:postscript[@place = 'margin_left']" mode="diplo">
        <div class="postscriptum ml-2">
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:postscript[@place = 'margin_right']" mode="diplo">
        <div class="postscriptum mr-2">
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:postscript[@place = 'top_letter']" mode="diplo_top_letter">
        <div class="postscriptum">
            <xsl:apply-templates mode="diplo"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:postscript[@place = 'top_page']" mode="diplo_top_page">
        <div class="postscriptum">
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:dateline" mode="diplo linear">
        <p>
            <xsl:apply-templates mode="#current"/>
        </p>
    </xsl:template>
    <xsl:template match="tei:salute" mode="diplo linear">
        <p class="salute">
            <xsl:choose>
                <xsl:when test="@rend = 'alinea'">
                    <xsl:attribute name="style">
                        <xsl:text>text-indent: 20px;</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates mode="#current"/>
                </xsl:when>
                <xsl:when test="@rend = 'center'">
                    <xsl:attribute name="class">
                        <xsl:text>text-center</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates mode="#current"/>
                </xsl:when>
                <xsl:when test="@rend = 'right'">
                    <xsl:attribute name="class">
                        <xsl:text>text-end</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates mode="#current"/>
                </xsl:when>
                <xsl:when test="@rend = 'left'">
                    <xsl:attribute name="class">
                        <xsl:text>text-start</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates mode="#current"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates mode="#current"/>
                </xsl:otherwise>
            </xsl:choose>
        </p>
    </xsl:template>
    <xsl:template match="tei:address" mode="diplo linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:addrLine" mode="diplo">
        <p class="m-0">
            <xsl:apply-templates mode="#current"/>
            <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
        </p>
    </xsl:template>
    <xsl:template match="tei:addrLine" mode="linear">
        <xsl:apply-templates mode="#current"/>
        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
    </xsl:template>
    <xsl:template match="tei:signed" mode="diplo linear">
        <p class="signed">
            <xsl:choose>
                <xsl:when test="@rend = 'alinea'">
                    <xsl:attribute name="style">
                        <xsl:text>text-indent: 20px;</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates mode="#current"/>
                </xsl:when>
                <xsl:when test="@rend = 'center'">
                    <xsl:attribute name="class">
                        <xsl:text>text-center</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates mode="#current"/>
                </xsl:when>
                <xsl:when test="@rend = 'right'">
                    <xsl:attribute name="class">
                        <xsl:text>text-end</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates mode="#current"/>
                </xsl:when>
                <xsl:when test="@rend = 'left'">
                    <xsl:attribute name="class">
                        <xsl:text>text-start</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates mode="#current"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates mode="#current"/>
                </xsl:otherwise>
            </xsl:choose>
        </p>
    </xsl:template>

    <xsl:template match="tei:date" mode="diplo linear note">
        <span>
            <xsl:choose>
                <xsl:when test="tei:note[not(@n)]">
                    <xsl:attribute name="title">
                        <xsl:apply-templates select="tei:note[not(@n)]" mode="title"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="@when and not(@when = '')">
                    <xsl:attribute name="title">
                        <xsl:value-of select="substring(@when, 7, 2)"/>
                        <xsl:text>/</xsl:text>
                        <xsl:value-of select="substring(@when, 5, 2)"/>
                        <xsl:text>/</xsl:text>
                        <xsl:value-of select="substring(@when, 1, 4)"/>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:title" mode="diplo linear note">
        <xsl:variable name="elem">
            <xsl:choose>
                <xsl:when test="tei:note[not(@n)]">
                    <xsl:text>mark</xsl:text>
                </xsl:when>
                <xsl:when test="@rend = 'desc'">
                    <xsl:text>span</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>i</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="@rend = 'desc'">
                <xsl:element name="{$elem}">
                    <xsl:if test="tei:note[not(@n)]">
                        <xsl:attribute name="title">
                            <xsl:apply-templates select="tei:note[not(@n)]" mode="title"/>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:apply-templates mode="#current"/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="{$elem}">
                    <i>
                        <xsl:if test="@type = 'titre_presse'">
                            <xsl:attribute name="title">
                                <xsl:text>Périodique</xsl:text>
                            </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="tei:note[not(@n)]">
                            <xsl:attribute name="title">
                                <xsl:apply-templates select="tei:note[not(@n)]" mode="title"/>
                            </xsl:attribute>
                        </xsl:if>
                        <xsl:apply-templates mode="#current"/>
                    </i>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:p" mode="diplo linear">
        <p>
            <xsl:if test="@rend">
                <xsl:attribute name="class">
                    <xsl:choose>
                        <xsl:when test="@rend = 'de_bas_en_haut'">
                            <xsl:text>rotate-sm-r-90</xsl:text>
                        </xsl:when>
                        <xsl:when test="@rend = 'de_haut_en_bas'">
                            <xsl:text>rotate-sm-l-90</xsl:text>
                        </xsl:when>
                        <xsl:when test="@rend = 'tete_beche'">
                            <xsl:text>rotate-sm-l-180</xsl:text>
                        </xsl:when>
                        <xsl:when test="@rend = 'diagonale_montante'">
                            <xsl:text>rotate-sm-l-45</xsl:text>
                        </xsl:when>
                        <xsl:when test="@rend = 'diagonale_descendante'">
                            <xsl:text>rotate-sm-r-45</xsl:text>
                        </xsl:when>
                        <xsl:when test="@rend = 'aligne_centre'">
                            <xsl:text>text-center</xsl:text>
                        </xsl:when>
                        <xsl:when test="@rend = 'aligne_droite'">
                            <xsl:text>text-end</xsl:text>
                        </xsl:when>
                        <xsl:when test="@rend = 'aligne_gauche'">
                            <xsl:text/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:attribute>
                <xsl:if test="@rend = 'alinea'">
                    <xsl:attribute name="style">
                        <xsl:text>text-indent: 20px;</xsl:text>
                    </xsl:attribute>
                </xsl:if>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </p>
    </xsl:template>
    <xsl:template match="tei:lb" mode="diplo note">
        <!-- Si j'utilise simplement <br/>, ça me génére deux <br/>, d'où l'utilisable de xsl:text suivant -->
        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
    </xsl:template>
    <xsl:template match="tei:lb[@type = 'hyphen']" mode="diplo note">
        <xsl:text>-</xsl:text>
        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
    </xsl:template>
    <xsl:template match="tei:lb" mode="linear">
        <xsl:text> </xsl:text>
    </xsl:template>
    <xsl:template match="tei:lb[@type = 'hyphen' or @break = 'no']" mode="linear"/>

    <!-- TODO : Sparadra, mais encodage à faire évoluer !!!
    AVANT :
    an<metamark rend="tiret_proustien" function="new_line">-</metamark><lb/>nnées
    APRES :
    an<metamark rend="tiret_proustien" function="new_line">-</metamark>nnées

    En l'occurrence, cf. lettre 03024, c'est même un peu plus compliqué car les lignes débutent par «, qui devrait à mon sens être encodé ainsi :
    <lb rend="&laquo;"/> (plutôt que par un <choice/> avec <sic/> : « et <corr/> vide
    (rem. déclaration préalable de l'entité &laquo; : <!ENTITY laquo "&#171;">)
    Si on fait valser les <lb/>... que faire de ce cas...
    -->
    <xsl:template
        match="tei:lb[preceding-sibling::*[position() = 1 and name() = 'metamark' and @function = 'new_line'] and text()[following-sibling::node() = current()]]"
        mode="linear diplo"/>

    <xsl:template name="fw_title">
        <xsl:attribute name="title">
            <xsl:text>Numéro de </xsl:text>
            <xsl:choose>
                <xsl:when test="@type = 'pageNum'">
                    <xsl:text>page</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@type"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:template>
    <xsl:template match="tei:fw[@place = 'top_center' or not(@place)]" mode="diplo">
        <p class="text-center">
            <xsl:call-template name="fw_title"/>
            <xsl:apply-templates mode="#current"/>
        </p>
    </xsl:template>
    <xsl:template match="tei:fw[@place = 'top_right']" mode="diplo">
        <p class="text-end">
            <xsl:call-template name="fw_title"/>
            <xsl:apply-templates mode="#current"/>
        </p>
    </xsl:template>
    <xsl:template match="tei:fw[@place = 'top_left']" mode="diplo">
        <p class="text-start">
            <xsl:call-template name="fw_title"/>
            <xsl:apply-templates mode="#current"/>
        </p>
    </xsl:template>
    <xsl:template match="tei:fw[@place = 'bottom_right']" mode="diplo"/>
    <xsl:template match="tei:fw[@place = 'bottom_right']" mode="diplo_bottom_page">
        <p class="text-end">
            <xsl:call-template name="fw_title"/>
            <xsl:apply-templates mode="diplo"/>
        </p>
    </xsl:template>
    <xsl:template match="tei:fw[@type = 'pageNum']" mode="linear"/>
    <xsl:template match="tei:fw" mode="linear"/>

    <!--   il s’agit du trait d’union que Proust utilise en fin de ligne lorsqu’il coupe
        un mot. Il faut baliser ce signe (et ce signe uniquement) avec « tiret_proustien »,
        afin que le mot ne se trouve pas coupé (ou affublé d’un trait d’union) lors de
        l’affichage de la transcription linéarisée sur le site web. -->
    <xsl:template match="tei:metamark[@rend = 'tiret_proustien']" mode="diplo">
        <span title="tiret proustien">-</span>
        <!-- Si j'utilise simplement <br/>, ça me génére deux <br/>, d'où l'utilisable de xsl:text suivant -->
        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
    </xsl:template>
    <xsl:template match="tei:metamark[@rend = 'tiret_proustien']" mode="linear"/>
    <!-- Proust écrit « . — . » à la fin d'une phrase pour marquer un nouveau paragraphe. -->
    <xsl:template match="tei:metamark[@rend = 'paragraphe_proustien']" mode="diplo">
        <span title="paragraphe proustien">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:metamark[@rend = 'paragraphe_proustien']" mode="linear">
        <xsl:text disable-output-escaping="yes">
            &lt;/p>&lt;p>
        </xsl:text>
    </xsl:template>
    <!-- TODO ou pas... car pas d'occurence connues...
    <xsl:template match="tei:metamark[@rend = 'trait_proustien']" mode="diplo linear">
        <span title="trait proustien">TODO</mark>
    </xsl:template>-->
    <xsl:template match="tei:metamark[@rend = 'ligne']" mode="diplo">
        <hr class="trait-proustien"/>
    </xsl:template>
    <xsl:template match="tei:metamark[@rend = 'ligne']" mode="linear"/>

    <xsl:template match="tei:figure" mode="diplo">
        <p class="m-auto col-8 m-3 p-2 border droit">
            <span class="md-18 material-icons">image</span>
            <xsl:if test="tei:head">
                <i>
                    <xsl:value-of select="tei:head"/>
                </i>
                <xsl:if test="tei:figDesc">
                    <xsl:text>, </xsl:text>
                </xsl:if>
            </xsl:if>
            <xsl:if test="tei:figDesc">
                <xsl:text>[</xsl:text>
                <xsl:apply-templates mode="#current" select="tei:figDesc"/>
                <xsl:text>]</xsl:text>
            </xsl:if>
        </p>
    </xsl:template>
    <xsl:template match="tei:figDesc" mode="diplo">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:figure" mode="linear"/>

    <xsl:template match="tei:hi" mode="linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'italic']" mode="diplo note linear">
        <i>
            <xsl:apply-templates mode="#current"/>
        </i>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'sup']" mode="diplo note linear">
        <sup>
            <xsl:apply-templates mode="#current"/>
        </sup>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'souligné']" mode="diplo note">
        <u>
            <xsl:apply-templates mode="#current"/>
        </u>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'souligné']" mode="linear">
        <i>
            <xsl:apply-templates mode="#current"/>
        </i>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'Souligné_double']" mode="diplo note linear">
        <span class="double-underline">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'alinéa']" mode="diplo note linear">
        <span class="ml-4">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:lg" mode="diplo linear">
        <blockquote>
            <xsl:apply-templates mode="#current"/>
        </blockquote>
    </xsl:template>
    <xsl:template match="tei:l" mode="diplo linear">
        <span class="d-block">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:persName" mode="diplo linear note">
        <xsl:choose>
            <xsl:when test="@ref and not(@ref = '')">
                <mark class="person-popover" data-bs-container="body" data-bs-toggle="popover"
                    data-bs-content="..." data-bs-trigger="hover focus" data-bs-html="true"
                    data-bs-custom-class="custom-popover" data-key="{substring-after(@ref,'#')}">
                    <xsl:apply-templates mode="#current"/>
                    <!--<xsl:if test="@type = 'fictif'">
                        <sup>
                            <i class="fa fa_info" title="Personnage fictif"/>
                        </sup>
                    </xsl:if>-->
                </mark>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates mode="#current"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:name" mode="diplo linear">
        <mark title="Nom">
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:orgName" mode="diplo linear note">
        <mark>
            <xsl:attribute name="title">
                <xsl:choose>
                    <xsl:when test="not(@corresp) or @corresp= ''">
                        <xsl:text>Nom d'établissement, institution, organisation ou autre</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="normalize-space(@corresp)"/> 
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:placeName" mode="diplo linear note">
        <mark title="Lieu">
            <xsl:if test="tei:note[not(@n)]">
                <xsl:attribute name="title">
                    <xsl:apply-templates select="tei:note[not(@n)]" mode="title"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </mark>
        <!--<xsl:if test="@type = 'fictif'">
            <sup>
                <i class="fa fa_info" title="Lieu fictif"/>
            </sup>
        </xsl:if>-->
    </xsl:template>

    <xsl:template match="tei:del[not(@rend)]" mode="diplo note">
        <s style="color:black">
            <span class="deleted-text">
                <xsl:apply-templates mode="#current"/>
            </span>
        </s>
    </xsl:template>
    <xsl:template match="tei:del[@rend]" mode="diplo note">
        <s style="color:purple">
            <xsl:attribute name="style">
                <xsl:text>color:</xsl:text>
                <xsl:choose>
                    <xsl:when test="@rend = 'encre_violette'">
                        <xsl:text>purple</xsl:text>
                    </xsl:when>
                    <xsl:when test="@rend = 'encre_bleue'">
                        <xsl:text>blue</xsl:text>
                    </xsl:when>
                    <xsl:when test="@rend = 'encre_rouge'">
                        <xsl:text>red</xsl:text>
                    </xsl:when>
                    <xsl:when test="@rend = 'encre_verte'">
                        <xsl:text>green</xsl:text>
                    </xsl:when>
                    <xsl:when test="@rend = 'crayon_graphique'">
                        <xsl:text>grey</xsl:text>
                    </xsl:when>
                    <xsl:when
                        test="@rend = 'encre_noire' or @rend = 'crayon_couleur' or @rend = 'mixte'">
                        <xsl:text>black</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>black</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>;</xsl:text>
            </xsl:attribute>
            <span class="deleted-text">
                <xsl:apply-templates mode="#current"/>
            </span>
        </s>
    </xsl:template>
    <xsl:template match="tei:del" mode="linear"/>
    <xsl:template match="tei:add[@place = 'top_page']" mode="diplo_top_page">
        <span title="ajout">
            <xsl:apply-templates mode="diplo"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:add[@place = 'top_page']" mode="diplo linear"/>
    <xsl:template match="tei:add" mode="diplo">
        <xsl:choose>
            <xsl:when test="@place = 'overwritting'">
                <sup title="ajout">
                    <xsl:apply-templates mode="#current"/>
                </sup>
            </xsl:when>
            <xsl:when test="@place = 'below'">
                <sub title="ajout">
                    <xsl:apply-templates mode="#current"/>
                </sub>
            </xsl:when>
            <xsl:when test="@place = 'margin_left'">
                <span title="ajout" class="col-2 fs-6"
                    style="margin-left:-15%; float:left; text-indent:0;">
                    <small>
                        <xsl:apply-templates mode="#current"/>
                    </small>
                </span>
            </xsl:when>
            <!-- TODO : avant on différenciait les ajouts interlinéaires supérieurs/inférieurs.
                Là ce n'est plus le cas. Par défaut : je suppose ajout interlinéaire.
                cf. 02830 "(et je ne crois pas)" (plutôt joli) et l'apostrophe deux lignes
                plus haut (avant certificat) qui s'affiche mal... -->
            <xsl:otherwise>
                <span class="interlinear-add">
                    <xsl:apply-templates mode="#current"/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:add" mode="linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:subst[tei:add and tei:del]" mode="diplo linear">
        <span>
            <xsl:apply-templates mode="diplo"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:choice[tei:sic and tei:corr]" mode="diplo">
        <span class="sic" title="Forme corrigée : {tei:corr}">
            <xsl:apply-templates mode="diplo" select="tei:sic"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:choice[tei:sic and tei:corr]" mode="linear">
        <xsl:choose>
            <xsl:when test="tei:sic = '«' and tei:corr = ''"/>
            <xsl:otherwise>
                <span class="corr" title="Forme d'origine : {tei:sic}">
                    <xsl:apply-templates mode="linear" select="tei:corr"/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:sic | tei:corr" mode="diplo linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:gap" mode="diplo linear">
        <span class="gap" title="Omission dans la transcription">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:unclear" mode="diplo">
        <u class="unclear" title="Texte incertain" style="text-decoration: underline wavy;">
            <xsl:apply-templates mode="#current"/>
        </u>
    </xsl:template>
    <xsl:template match="tei:unclear" mode="linear">
        <span title="Texte incertain">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:foreign" mode="diplo linear">
        <i>
            <xsl:attribute name="title">
                <xsl:if test="tei:note[not(@n)]">
                    <xsl:apply-templates select="tei:note[not(@n)]" mode="title"/>
                </xsl:if>
                <xsl:text>(emprunt </xsl:text>
                <xsl:value-of select="@xml:lang"/>
                <xsl:text>)</xsl:text>
            </xsl:attribute>
            <xsl:apply-templates mode="#current"/>
        </i>
    </xsl:template>

    <xsl:template match="tei:seg[@type = 'titre_financier']" mode="diplo linear">
        <span title="titre financier">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:seg[@type = 'titre_envisagé']" mode="diplo linear note">
        <span class="intended-title">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:seg[not(@type)]" mode="diplo linear">
        <span>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:cit" mode="diplo linear note">
        <span>
            <xsl:attribute name="title">
                <xsl:choose>
                    <xsl:when test="tei:quote[@type = 'short']">
                        <xsl:text>Citation partielle</xsl:text>
                    </xsl:when>
                    <xsl:when test="tei:quote[@type = 'desc']">
                        <xsl:text>Citation approximative</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>Citation</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="tei:ref">
                    <xsl:text> de </xsl:text>
                    <xsl:apply-templates select="tei:ref" mode="title"/>
                </xsl:if>
            </xsl:attribute>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:cit/tei:ref" mode="diplo linear"/>
    <!-- Déjà géré dans le template matchant tei:ref -->

    <xsl:template match="tei:quote" mode="diplo linear note">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:ref" mode="title">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:ref[not(parent::tei:cite) and @type = 'reference_externe']"
        mode="diplo linear note">
        <a href="{@target}" target="_blank">
            <xsl:apply-templates mode="#current"/>
        </a>
    </xsl:template>
    <xsl:template match="tei:ref[not(parent::tei:cite) and @type = 'renvoi']"
        mode="diplo linear note">
        <a href="/letter/{@target}" target="_blank">
            <xsl:apply-templates mode="#current"/>
        </a>
    </xsl:template>

    <xsl:template match="tei:note[@n]" mode="diplo linear">
        <sup class="note-call" aria-hidden="true" data-note-id="n{@n}">
            <xsl:value-of select="@n"/>
        </sup>
    </xsl:template>
    <xsl:template match="tei:note[not(@n)]" mode="diplo linear note"/>
    <xsl:template match="tei:note" mode="title">
        <xsl:choose>
            <xsl:when test="count(child::*) = 1">
                <xsl:choose>
                    <xsl:when test="tei:bibl">
                        <xsl:apply-templates select="tei:bibl" mode="title"/>
                        <!--<xsl:if test="tei:author">
                            <xsl:value-of select="tei:author"/>
                            <xsl:text> </xsl:text>
                        </xsl:if>
                        <xsl:if test="tei:date">
                            <xsl:text>(</xsl:text>
                            <xsl:value-of select="tei:date"/>
                            <xsl:text>) </xsl:text>
                        </xsl:if>
                        <xsl:value-of select="tei:title"/>
                        <xsl:text>.</xsl:text>-->
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates mode="#current"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates mode="#current"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:listBibl" mode="title">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:bibl" mode="title">
        <xsl:if test="tei:author">
            <xsl:value-of select="tei:author"/>
        </xsl:if>
        <xsl:if test="tei:author and tei:title[@level = 'main']">
            <xsl:text>, </xsl:text>
            <xsl:value-of select="tei:title[@level = 'main']"/>
        </xsl:if>
        <xsl:if test="tei:title[@level = 'sub']">
            <xsl:text>, «&non_breakable_space;</xsl:text>
            <xsl:value-of select="tei:title[@level = 'sub']"/>
            <xsl:text>&non_breakable_space;»</xsl:text>
        </xsl:if>
        <xsl:if test="tei:publisher">
            <xsl:text>, </xsl:text>
            <xsl:value-of select="tei:publisher"/>
        </xsl:if>
        <xsl:if test="tei:pubPlace">
            <xsl:text>, </xsl:text>
            <xsl:value-of select="tei:pubPlace"/>
        </xsl:if>
        <xsl:if test="tei:date">
            <xsl:text>, </xsl:text>
            <xsl:value-of select="tei:date"/>
        </xsl:if>
        <xsl:if test="tei:biblScope">
            <xsl:for-each select="tei:biblScope">
                <xsl:text>, </xsl:text>
                <xsl:choose>
                    <xsl:when test="@type = 'page'">
                        <xsl:text>p.</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@type"/>
                        <xsl:text> </xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select="."/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="count(child::*) > 0">
            <xsl:text>.</xsl:text>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:title" mode="title">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="*" mode="title">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="text()" mode="title">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>
    <!--<xsl:template match="tei:note[not(@n)]" mode="diplo linear note">
        <xsl:variable name="num">
            <xsl:number count="." level="any"/>
        </xsl:variable>
        <xsl:if test="parent::tei:del">
            <xsl:text disable-output-escaping="yes">&lt;/s></xsl:text>
        </xsl:if>
        <sup class="note-call note-call-other" aria-hidden="true" data-note-id="n{$num}">
            <xsl:text>i</xsl:text>
        </sup>
        <xsl:if test="parent::tei:del">
            <xsl:text disable-output-escaping="yes">&lt;s></xsl:text>
        </xsl:if>
    </xsl:template>-->

    <!-- TODO : liste issue de l'ODD :
    * diplo : handShift figure figDesc head seg date address addrLine addName forename surname term
    * dans le teiHeader on a aussi : keywords textClass correspAction correspDesc profileDesc accMat handNote handDesc
    measure width height dimensions extent objectType support supportDesc objectDesc
    physDesc idno collection repository institution
    msIdentifier msDesc sourceDesc biblScope pubPlace publisher editor author
    listBibl bibl notesStmt relatedItem publicationStmt name resp respStmt title titleStmt fileDesc teiHeader
    -->

    <!-- Query 'json' -->
    <xsl:template match="tei:teiHeader" mode="json">
        <xsl:variable name="xml-files" select="collection(concat($xml-dir, '/.?select=*_*.xml'))"/>
        <xsl:call-template name="create_json">
            <xsl:with-param name="xml-files" select="$xml-files"/>
        </xsl:call-template>
    </xsl:template>

    <!-- Query 'json_one' -->
    <xsl:template match="tei:teiHeader" mode="json_one">
        <xsl:call-template name="create_json">
            <xsl:with-param name="xml-files" select="root()"/>
        </xsl:call-template>
    </xsl:template>

    <!-- Query 'json_search' -->
    <xsl:template match="tei:body" mode="json_search">
        <xsl:variable name="xml-files" select="collection(concat($xml-dir, '/.?select=*_*.xml'))"/>
        <xsl:variable name="xml-ok">
            <xsl:for-each select="$xml-files">
                <xsl:if
                    test="count(//tei:body//tei:*/text()[contains(lower-case(.), lower-case($search_word)) and not(parent::tei:note)]) > 0">
                    <xsl:copy-of select="."/>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="create_json">
            <xsl:with-param name="xml-files" select="$xml-ok"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="create_json">
        <xsl:param name="xml-files"/>
        <xsl:text>[</xsl:text>
        <xsl:for-each select="$xml-files">
            <xsl:choose>
                <xsl:when test="not(name(./*) = 'TEI')"/>
                <xsl:otherwise>
                    <xsl:variable name="id">
                        <xsl:value-of
                            select="substring-after(./tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/@xml:id, 'cp')"
                        />
                    </xsl:variable>
                    <xsl:variable name="from_id">
                        <xsl:value-of
                            select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:persName/@ref"
                        />
                    </xsl:variable>
                    <xsl:variable name="from">
                        <xsl:apply-templates
                            select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:persName"
                            mode="#current"/>
                    </xsl:variable>
                    <xsl:variable name="from_canonical_name">
                        <xsl:apply-templates
                            select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:persName"
                            mode="canonical"/>
                    </xsl:variable>
                    <xsl:variable name="to_id">
                        <xsl:value-of
                            select="tei:TEI/tei:teiHeader/tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'received']/tei:persName/@ref"
                        />
                    </xsl:variable>
                    <xsl:variable name="to">
                        <xsl:apply-templates
                            select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'received']/tei:persName"
                            mode="#current"/>
                    </xsl:variable>
                    <xsl:variable name="to_canonical_name">
                        <xsl:apply-templates
                            select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'received']/tei:persName"
                            mode="canonical"/>
                    </xsl:variable>
                    <xsl:variable name="date">
                        <xsl:value-of
                            select="normalize-space(tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date)"
                        />
                    </xsl:variable>
                    <xsl:variable name="tags">
                        <xsl:for-each
                            select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:textClass/tei:keywords/tei:term[not(. = '')]">
                            <xsl:text xml:space="preserve">
    "</xsl:text>
                            <xsl:value-of select="normalize-space(.)"/>
                            <xsl:text>"</xsl:text>
                            <xsl:if test="last() > position()">
                                <xsl:text>,</xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:variable name="lower">
                        <xsl:choose>
                            <xsl:when test="
                                    tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@when
                                    and not(tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@when = '')">
                                <xsl:value-of
                                    select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@when"
                                />
                            </xsl:when>
                            <xsl:when test="
                                    tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atLeast
                                    and not(tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atLeast = '')">
                                <xsl:value-of
                                    select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atLeast"
                                />
                            </xsl:when>
                            <xsl:when test="
                                    tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atMost
                                    and not(tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atMost = '')">
                                <xsl:value-of
                                    select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atMost"
                                />
                            </xsl:when>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="lower-formated">
                        <xsl:if test="$lower and not($lower = '')">
                            <xsl:value-of select="substring($lower, 1, 4)"/>
                            <xsl:text>-</xsl:text>
                            <xsl:value-of select="substring($lower, 5, 2)"/>
                            <xsl:text>-</xsl:text>
                            <xsl:choose>
                                <xsl:when
                                    test="string-length($lower) = 8 and xs:integer(substring($lower, 7, 2)) > 1 and xs:integer(substring($lower, 7, 2)) &lt; 31">
                                    <xsl:value-of select="substring($lower, 7, 2)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>01</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:text>T00:00:00</xsl:text>
                        </xsl:if>
                    </xsl:variable>
                    <xsl:variable name="higher">
                        <xsl:choose>
                            <xsl:when test="
                                    tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@when
                                    and not(tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@when = '')">
                                <xsl:value-of
                                    select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@when"
                                />
                            </xsl:when>
                            <xsl:when test="
                                    tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atMost
                                    and not(tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atMost = '')">
                                <xsl:value-of
                                    select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atMost"
                                />
                            </xsl:when>
                            <xsl:when test="
                                    tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atLeast
                                    and not(tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atLeast = '')">
                                <xsl:value-of
                                    select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atLeast"
                                />
                            </xsl:when>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="higher-formated">
                        <xsl:if test="$higher and not($higher = '')">
                            <xsl:value-of select="substring($higher, 1, 4)"/>
                            <xsl:text>-</xsl:text>
                            <xsl:value-of select="substring($higher, 5, 2)"/>
                            <xsl:text>-</xsl:text>
                            <xsl:choose>
                                <xsl:when
                                    test="string-length($higher) = 8 and xs:integer(substring($higher, 7, 2)) > 1 and xs:integer(substring($higher, 7, 2)) &lt; 31">
                                    <xsl:value-of select="substring($higher, 7, 2)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>01</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:text>T00:00:00</xsl:text>
                        </xsl:if>
                    </xsl:variable>
                    <xsl:variable name="lower-for-json">
                        <xsl:if test="$lower-formated and not($lower-formated = '')">
                            <xsl:value-of
                                select="days-from-duration(xs:dateTime($lower-formated) - xs:dateTime('1970-01-01T00:00:00')) * 24 * 60 * 60 * 1000"
                            />
                        </xsl:if>
                    </xsl:variable>
                    <xsl:variable name="higher-for-json">
                        <xsl:if test="$higher-formated and not($higher-formated = '')">
                            <xsl:value-of
                                select="days-from-duration(xs:dateTime($higher-formated) - xs:dateTime('1970-01-01T00:00:00')) * 24 * 60 * 60 * 1000"
                            />
                        </xsl:if>
                    </xsl:variable>
                    <xsl:text xml:space="preserve">
{
  "id": "</xsl:text>
                    <xsl:value-of select="$id"/>
                    <xsl:text xml:space="preserve">",
  "from": "</xsl:text>
                    <xsl:value-of select="$from"/>
                    <xsl:text xml:space="preserve">",
  "to": "</xsl:text>
                    <xsl:value-of select="$to"/>
                    <xsl:text xml:space="preserve">",
  "from_id": "</xsl:text>
                    <xsl:value-of select="$from_id"/>
                    <xsl:text xml:space="preserve">",
  "from_canonical": "</xsl:text>
                    <xsl:value-of select="$from_canonical_name"/>
                    <xsl:text xml:space="preserve">",
  "to_id": "</xsl:text>
                    <xsl:value-of select="$to_id"/>
                    <xsl:text xml:space="preserve">",
  "to_canonical": "</xsl:text>
                    <xsl:value-of select="$to_canonical_name"/>
                    <xsl:text xml:space="preserve">",
  "date": "</xsl:text>
                    <xsl:value-of select="$date"/>
                    <xsl:text xml:space="preserve">",
  "tags": [</xsl:text>
                    <xsl:value-of select="$tags"/>
                    <xsl:text xml:space="preserve">
  ],
  "boundaries": {
    "lower": </xsl:text>
                    <xsl:value-of select="$lower-for-json"/>
                    <xsl:text xml:space="preserve">,
    "higher": </xsl:text>
                    <xsl:value-of select="$higher-for-json"/>
                    <xsl:text xml:space="preserve">
  }
}</xsl:text>
                    <xsl:if test="last() > position()">
                        <xsl:text>,</xsl:text>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        <xsl:text>]</xsl:text>
    </xsl:template>

    <xsl:template match="tei:persName" mode="canonical">
        <span data-link="https://kolbproust-nd.library.illinois.edu/{@ref}.jsonld">
            <xsl:choose>
                <xsl:when test="tei:addName">
                    <xsl:apply-templates select="tei:addName" mode="#current"/>
                </xsl:when>
                <xsl:when test="tei:surname and tei:forename">
                    <xsl:value-of select="normalize-space(tei:surname)"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="normalize-space(tei:forename)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(.)"/>
                </xsl:otherwise>
            </xsl:choose>
        </span>
    </xsl:template>
    <xsl:template match="tei:addName" mode="canonical">
        <xsl:choose>
            <xsl:when test="tei:surname and tei:forename">
                <xsl:value-of select="normalize-space(tei:surname)"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="normalize-space(tei:forename)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space(.)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Query 'metadata' -->
    <xsl:template match="tei:teiHeader" mode="metadata">
        <xsl:variable name="metadata" as="node()">
            <tei:metadata>
                <tei:group xml:id="received" title="Destinataire">
                    <xsl:if
                        test="exists(tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'received'])">
                        <xsl:attribute name="subtitle">
                            <xsl:value-of
                                select="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'received']/tei:persName/@ref"
                            />
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:if
                        test="exists(tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'non_sent'])">
                        <tei:attval>
                            <tei:att>
                                <xsl:text>Remarque</xsl:text>
                            </tei:att>
                            <tei:val>
                                <xsl:text>Lettre non envoyée</xsl:text>
                            </tei:val>
                        </tei:attval>
                    </xsl:if>
                    <xsl:if
                        test="exists(tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'received'])">
                        <tei:attval>
                            <tei:att>
                                <xsl:text>Nom</xsl:text>
                            </tei:att>
                            <tei:val>
                                <xsl:value-of
                                    select="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'received']/tei:persName/tei:surname"
                                />
                            </tei:val>
                        </tei:attval>
                        <tei:attval>
                            <tei:att>
                                <xsl:text>Prénom</xsl:text>
                            </tei:att>
                            <tei:val>
                                <xsl:value-of
                                    select="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'received']/tei:persName/tei:forename"
                                />
                            </tei:val>
                        </tei:attval>
                    </xsl:if>
                </tei:group>
                <tei:group xml:id="send" title="Expéditeur"
                    subtitle="{tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:persName/@ref}">
                    <tei:attval>
                        <tei:att>
                            <xsl:text>Nom</xsl:text>
                        </tei:att>
                        <tei:val>
                            <xsl:value-of
                                select="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:persName/tei:surname"
                            />
                        </tei:val>
                    </tei:attval>
                    <tei:attval>
                        <tei:att>
                            <xsl:text>Prénom</xsl:text>
                        </tei:att>
                        <tei:val>
                            <xsl:value-of
                                select="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:persName/tei:forename"
                            />
                        </tei:val>
                    </tei:attval>
                    <tei:attval>
                        <tei:att>
                            <xsl:text>Adresse</xsl:text>
                        </tei:att>
                        <tei:val>
                            <xsl:value-of
                                select="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:address/tei:addrLine"
                            />
                        </tei:val>
                    </tei:attval>
                    <tei:attval>
                        <tei:att>
                            <xsl:text>Signature</xsl:text>
                        </tei:att>
                        <tei:val>
                            <xsl:choose>
                                <xsl:when test="../tei:text//tei:closer/tei:signed">
                                    <xsl:text>Oui (</xsl:text>
                                    <i class="text-muted">
                                        <xsl:value-of select="../tei:text//tei:closer/tei:signed"/>
                                    </i>
                                    <xsl:text>)</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>Non</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </tei:val>
                    </tei:attval>
                </tei:group>
                <tei:group xml:id="write" title="Rédaction">
                    <tei:attval>
                        <tei:att>Date</tei:att>
                        <tei:val>
                            <xsl:value-of
                                select="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date"
                            />
                        </tei:val>
                    </tei:attval>
                    <!--
                    <xsl:if
                        test="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@when">
                        <tei:attval>
                            <tei:att>Date</tei:att>
                            <tei:val>
                                <xsl:variable name="when"
                                    select="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@when"/>
                                <xsl:value-of select="substring($when, 7, 2)"/>
                                <xsl:text>/</xsl:text>
                                <xsl:value-of select="substring($when, 5, 2)"/>
                                <xsl:text>/</xsl:text>
                                <xsl:value-of select="substring($when, 1, 4)"/>
                            </tei:val>
                        </tei:attval>
                    </xsl:if>
                    <xsl:if test="
                            tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atLeast
                            and tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atMost">
                        <tei:attval>
                            <tei:att>
                                <xsl:text>Entre le </xsl:text>
                            </tei:att>
                            <tei:val>
                                <xsl:variable name="atLeast"
                                    select="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atLeast"/>
                                <xsl:value-of select="substring($atLeast, 7, 2)"/>
                                <xsl:text>/</xsl:text>
                                <xsl:value-of select="substring($atLeast, 5, 2)"/>
                                <xsl:text>/</xsl:text>
                                <xsl:value-of select="substring($atLeast, 1, 4)"/>
                            </tei:val>
                        </tei:attval>
                        <tei:attval>
                            <tei:att>
                                <xsl:text> et le </xsl:text>
                            </tei:att>
                            <tei:val>
                                <xsl:variable name="atMost"
                                    select="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@atMost"/>
                                <xsl:value-of select="substring($atMost, 7, 2)"/>
                                <xsl:text>/</xsl:text>
                                <xsl:value-of select="substring($atMost, 5, 2)"/>
                                <xsl:text>/</xsl:text>
                                <xsl:value-of select="substring($atMost, 1, 4)"/>
                            </tei:val>
                        </tei:attval>
                    </xsl:if>
                    -->
                    <tei:attval>
                        <tei:att>
                            <xsl:text>Redatée</xsl:text>
                        </tei:att>
                        <tei:val>
                            <xsl:choose>
                                <xsl:when
                                    test="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date/@change = 'oui'">
                                    <xsl:text>Oui</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>Non</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </tei:val>
                    </tei:attval>
                </tei:group>
                <tei:group xml:id="publications" title="Publications antérieures"
                    set_title="Publication antérieure">
                    <xsl:for-each
                        select="tei:fileDesc/tei:notesStmt/tei:relatedItem/tei:listBibl/tei:bibl">
                        <tei:set>
                            <xsl:choose>
                                <xsl:when
                                    test="@corresp = '#Kolb' or @corresp = '#CG' or @corresp = '#Lettres'">
                                    <tei:attval>
                                        <tei:att>Source</tei:att>
                                        <tei:val>
                                            <xsl:choose>
                                                <xsl:when test="@corresp = '#CG'">
                                                  <xsl:text>Correspondance générale</xsl:text>
                                                </xsl:when>
                                                <xsl:when test="@corresp = '#Kolb'">
                                                  <xsl:text>Kolb</xsl:text>
                                                </xsl:when>
                                                <xsl:when test="@corresp = '#Lettres'">
                                                  <xsl:text>Lettres (1879-1922)</xsl:text>
                                                </xsl:when>
                                            </xsl:choose>
                                        </tei:val>
                                    </tei:attval>
                                    <xsl:if test="tei:biblScope[@unit = 'tome']">
                                        <tei:attval>
                                            <tei:att>Tome</tei:att>
                                            <tei:val>
                                                <xsl:value-of select="tei:biblScope[@unit = 'tome']"
                                                />
                                            </tei:val>
                                        </tei:attval>
                                    </xsl:if>
                                    <tei:attval>
                                        <tei:att>Numéro de lettre</tei:att>
                                        <tei:val>
                                            <xsl:value-of
                                                select="tei:biblScope[@unit = 'num_letter']"/>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Pages</tei:att>
                                        <tei:val>
                                            <xsl:value-of select="tei:biblScope[@unit = 'page']"/>
                                        </tei:val>
                                    </tei:attval>
                                </xsl:when>
                                <xsl:when test="@type = 'livre'">
                                    <tei:attval>
                                        <tei:att>Source</tei:att>
                                        <tei:val>
                                            <xsl:text>Ouvrage</xsl:text>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Titre</tei:att>
                                        <tei:val>
                                            <tei:title>
                                                <xsl:value-of select="tei:title[@level = 'main']"/>
                                                <xsl:if test="tei:title[@level = 'sub']">
                                                  <xsl:text>, «&non_breakable_space;</xsl:text>
                                                  <xsl:value-of select="tei:title[@level = 'sub']"/>
                                                  <xsl:text>&non_breakable_space;»</xsl:text>
                                                </xsl:if>
                                            </tei:title>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Date d'édition</tei:att>
                                        <tei:val>
                                            <xsl:value-of select="tei:date"/>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Auteur</tei:att>
                                        <tei:val>
                                            <xsl:value-of select="tei:author"/>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Édition</tei:att>
                                        <tei:val>
                                            <xsl:value-of select="tei:publisher"/>
                                        </tei:val>
                                    </tei:attval>
                                    <xsl:if test="tei:editor and not(tei:editor = '')">
                                        <tei:attval>
                                            <tei:att>Édition scientifique</tei:att>
                                            <tei:val>
                                                <xsl:value-of select="tei:editor"/>
                                            </tei:val>
                                        </tei:attval>
                                    </xsl:if>
                                    <tei:attval>
                                        <tei:att>Lieu d'édition</tei:att>
                                        <tei:val>
                                            <xsl:value-of select="tei:pubPlace"/>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Pages</tei:att>
                                        <tei:val>
                                            <xsl:value-of select="tei:biblScope[@unit = 'page']"/>
                                        </tei:val>
                                    </tei:attval>
                                </xsl:when>
                                <xsl:when test="@type = 'journal'">
                                    <tei:attval>
                                        <tei:att>Source</tei:att>
                                        <tei:val>
                                            <xsl:text>Journal</xsl:text>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Titre</tei:att>
                                        <tei:val>
                                            <tei:title>
                                                <xsl:value-of select="tei:title[@level = 'main']"/>
                                            </tei:title>
                                            <xsl:if test="tei:title[@level = 'sub']">
                                                <xsl:text>, «&non_breakable_space;</xsl:text>
                                                <xsl:value-of select="tei:title[@level = 'sub']"/>
                                                <xsl:text>&non_breakable_space;»</xsl:text>
                                            </xsl:if>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Date de publication</tei:att>
                                        <tei:val>
                                            <xsl:value-of select="tei:date"/>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Auteur</tei:att>
                                        <tei:val>
                                            <xsl:value-of select="tei:author"/>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Pages</tei:att>
                                        <tei:val>
                                            <xsl:value-of select="tei:biblScope[@unit = 'page']"/>
                                        </tei:val>
                                    </tei:attval>
                                </xsl:when>
                                <xsl:when test="@type = 'lettres'">
                                    <tei:attval>
                                        <tei:att>Source</tei:att>
                                        <tei:val>
                                            <xsl:text>Lettres publiées</xsl:text>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Numéro de lettre</tei:att>
                                        <tei:val>
                                            <xsl:value-of
                                                select="tei:biblScope[@unit = 'num_letter']"/>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Pages</tei:att>
                                        <tei:val>
                                            <xsl:value-of select="tei:biblScope[@unit = 'page']"/>
                                        </tei:val>
                                    </tei:attval>
                                </xsl:when>
				<xsl:when test="@type = 'catalogue'">
                                    <tei:attval>
                                        <tei:att>Source</tei:att>
                                        <tei:val>
                                            <xsl:text>Catalogue </xsl:text>
                                            <xsl:choose>
                                                <xsl:when test="@subtype = 'vente'">
                                                  <xsl:text> de vente</xsl:text>
                                                </xsl:when>
                                                <xsl:when test="@subtype = 'exposition'">
                                                  <xsl:text> d'exposition</xsl:text>
                                                </xsl:when>
                                            </xsl:choose>
                                        </tei:val>
                                    </tei:attval>
                                    <tei:attval>
                                        <tei:att>Catalogue</tei:att>
                                        <tei:val>
                                            <xsl:for-each select="tei:title">
                                                <xsl:choose>
                                                  <xsl:when test="@level = 'main'">
                                                  <i>
                                                  <xsl:value-of select="."/>
                                                  </i>
                                                  </xsl:when>
                                                  <xsl:when test="@level = 'sub'">
                                                  <xsl:text>, </xsl:text>
                                                  <xsl:value-of select="."/>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:value-of select="."/>
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:for-each>
                                            <xsl:text>, </xsl:text>
                                            <xsl:value-of select="tei:pubPlace"/>
                                            <xsl:text>, </xsl:text>
                                            <xsl:value-of select="tei:publisher"/>
                                            <xsl:text>, vol. </xsl:text>
                                            <xsl:value-of select="tei:biblScope[@unit = 'volume']"/>
                                            <xsl:text>, p. </xsl:text>
                                            <xsl:value-of select="tei:biblScope[@unit = 'page']"/>
                                            <xsl:text>, </xsl:text>
                                            <xsl:value-of select="tei:date"/>
                                            <xsl:text>.</xsl:text>
                                        </tei:val>
                                    </tei:attval>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:if test="tei:note">
                                <tei:attval>
                                    <tei:att>Note</tei:att>
                                    <tei:val>
                                        <xsl:value-of select="tei:note"/>
                                    </tei:val>
                                </tei:attval>
                            </xsl:if>
                        </tei:set>
                    </xsl:for-each>
                </tei:group>
                <tei:group xml:id="location" title="Localisation">
		    <xsl:choose>
                        <xsl:when
                            test="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:collection/@type">
                            <tei:attval>
                                <tei:att>Collection</tei:att>
                                <tei:val>
                                    <xsl:value-of
                                        select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:collection/@type"
                                    />
                                </tei:val>
                            </tei:attval>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="not(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:collection = '')"><tei:attval>
                                <tei:att>Collection</tei:att>
                                <tei:val>
                                    <xsl:value-of
                                        select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:collection/@type"
                                    />
                                </tei:val>
                            </tei:attval>
                            </xsl:if>
                            <xsl:if test="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:repository = 'localisation inconnue'"><tei:attval>
                                <tei:att>Information</tei:att>
                                <tei:val>
                                    <xsl:value-of
                                        select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:repository"
                                    />
                                </tei:val>
                            </tei:attval>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if
                        test="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:collection/@type = 'publique'">
                        <tei:attval>
                            <tei:att>Institution</tei:att>
                            <tei:val>
                                <xsl:choose>
                                    <xsl:when
                                        test="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:institution/@corresp = '#UIUC'">
                                        <xsl:text>University of Illinois at Urbana-Champaign</xsl:text>
                                    </xsl:when>
                                    <xsl:when
                                        test="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:institution/@corresp = '#BnF'">
                                        <xsl:text>Bibliothèque nationale de France, Paris, France</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of
                                            select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:institution"
                                        />
                                    </xsl:otherwise>
                                </xsl:choose>
                            </tei:val>
                        </tei:attval>
                        <xsl:if
                            test="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:collection">
                            <tei:attval>
                                <tei:att>Fonds</tei:att>
                                <tei:val>
                                    <xsl:choose>
                                        <xsl:when
                                            test="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:collection = 'RBML'">
                                            <xsl:text>Rare Book and Manuscript Library</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:collection"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </tei:val>
                            </tei:attval>
                        </xsl:if>
                        <xsl:if
                            test="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:repository">
                            <tei:attval>
                                <tei:att>Département</tei:att>
                                <tei:val>
                                    <xsl:value-of
                                        select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:repository"
                                    />
                                </tei:val>
                            </tei:attval>
                        </xsl:if>
                        <xsl:if
                            test="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno">
                            <tei:attval>
                                <tei:att>Cote</tei:att>
                                <tei:val>
                                    <xsl:value-of
                                        select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno"
                                    />
                                </tei:val>
                            </tei:attval>
                        </xsl:if>
                        <!--<tei:attval>
                            <tei:att>Pays</tei:att>
                            <tei:val>TODO</tei:val>
                        </tei:attval>
                        <tei:attval>
                            <tei:att>Ville</tei:att>
                            <tei:val>TODO</tei:val>
                        </tei:attval>-->
                    </xsl:if>
                </tei:group>
                <tei:group xml:id="physDesc" title="Propriétés physiques">
                    <tei:attval>
                        <tei:att>Intégralité</tei:att>
                        <tei:val>
                            <xsl:choose>
                                <xsl:when test="//tei:f[@name = 'entirety']/@fVal = 'false'">
                                    <xsl:text>Non</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>Oui</xsl:otherwise>
                            </xsl:choose>
                        </tei:val>
                    </tei:attval>
                    <tei:attval>
                        <tei:att>Lacunes</tei:att>
                        <tei:val>
                            <xsl:choose>
                                <xsl:when
                                    test="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:condition/tei:fs/tei:f[@name = 'lacuna' and @fVal = 'true']">
                                    <xsl:text>Oui</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>Non</xsl:otherwise>
                            </xsl:choose>
                        </tei:val>
                    </tei:attval>
                    <tei:attval>
                        <tei:att>Dessins</tei:att>
                        <tei:val>
                            <xsl:choose>
                                <xsl:when test="count(../tei:text/tei:body//tei:figure) > 0">
                                    <xsl:text>Oui</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>Non</xsl:otherwise>
                            </xsl:choose>
                        </tei:val>
                    </tei:attval>
                    <tei:attval>
                        <tei:att>Nombre de feuillets</tei:att>
                        <tei:val>
                            <xsl:value-of
                                select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:extent/tei:measure[@unit = 'feuillet']"
                            />
                        </tei:val>
                    </tei:attval>
                    <tei:attval>
                        <tei:att>Nombre de pages écrites</tei:att>
                        <tei:val>
                            <xsl:value-of
                                select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:extent/tei:measure[@unit = 'pages_écrites']"
                            />
                        </tei:val>
                    </tei:attval>
                    <xsl:if test="
                            tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:note
                            and not(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:note = '')">
                        <tei:attval>
                            <tei:att>Remarque</tei:att>
                            <tei:val>
                                <xsl:value-of
                                    select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:note"
                                />
                            </tei:val>
                        </tei:attval>
                    </xsl:if>
                </tei:group>
                <tei:group xml:id="hand" title="Écriture" set_title="Main">
                    <xsl:for-each
                        select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:handDesc">
                        <tei:set>
                            <tei:attval>
                                <tei:att>Scripteur</tei:att>
                                <tei:val>
                                    <!--<xsl:value-of select="tei:handNote/@scribe"/>-->
                                    <xsl:apply-templates
                                        select="../../../../../tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:persName"
                                        mode="canonical"/>
                                </tei:val>
                            </tei:attval>
                            <tei:attval>
                                <tei:att>Type</tei:att>
                                <tei:val>
                                    <xsl:value-of select="translate(tei:handNote/@script, '_', ' ')"
                                    />
                                </tei:val>
                            </tei:attval>
                            <tei:attval>
                                <tei:att>Medium</tei:att>
                                <tei:val>
                                    <xsl:value-of select="translate(tei:handNote/@medium, '_', ' ')"
                                    />
                                </tei:val>
                            </tei:attval>
                            <xsl:if test="tei:note and not(tei:note = '')">
                                <tei:attval>
                                    <tei:att>Remarque</tei:att>
                                    <tei:val>
                                        <xsl:value-of select="tei:note"/>
                                    </tei:val>
                                </tei:attval>
                            </xsl:if>
                        </tei:set>
                    </xsl:for-each>
                </tei:group>
                <tei:group xml:id="support" title="Supports">
                    <tei:attval>
                        <tei:att>Type</tei:att>
                        <tei:val>
                            <xsl:value-of
                                select="translate(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/@material, '_', ' ')"
                            />
                        </tei:val>
                    </tei:attval>
                    <xsl:if test="
                            tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:condition/tei:p
                            and not(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:condition/tei:p = '')">
                        <tei:attval>
                            <tei:att>Remarque</tei:att>
                            <tei:val>
                                <xsl:value-of
                                    select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:condition/tei:p"
                                />
                            </tei:val>
                        </tei:attval>
                    </xsl:if>
                </tei:group>
                <tei:group xml:id="paper" title="Papiers">
                    <xsl:choose>
                        <xsl:when
                            test="count(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:extent/tei:dimensions) = 0"/>
                        <xsl:when
                            test="count(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:extent/tei:dimensions) > 1">
                            <xsl:for-each
                                select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:extent/tei:dimensions">
                                <tei:attval>
                                    <tei:att>
                                        <xsl:choose>
                                            <xsl:when test="matches(@scope, '^[0-9]+$')">
                                                <xsl:text>Dimensions du feuillet </xsl:text>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:text>Dimensions des feuillets </xsl:text>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        <xsl:value-of select="@scope"/>
                                    </tei:att>
                                    <tei:val>
                                        <xsl:value-of select="tei:width"/>
                                        <xsl:text> </xsl:text>
                                        <xsl:value-of select="tei:width/@unit"/>
                                        <xsl:text> par </xsl:text>
                                        <xsl:value-of select="tei:height"/>
                                        <xsl:text> </xsl:text>
                                        <xsl:value-of select="tei:height/@unit"/>
                                    </tei:val>
                                </tei:attval>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <tei:attval>
                                <tei:att>Largeur</tei:att>
                                <tei:val>
                                    <xsl:value-of
                                        select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:extent/tei:dimensions/tei:width"/>
                                    <xsl:text> </xsl:text>
                                    <xsl:value-of
                                        select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:extent/tei:dimensions/tei:width/@unit"
                                    />
                                </tei:val>
                            </tei:attval>
                            <tei:attval>
                                <tei:att>Hauteur</tei:att>
                                <tei:val>
                                    <xsl:value-of
                                        select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:extent/tei:dimensions/tei:height"/>
                                    <xsl:text> </xsl:text>
                                    <xsl:value-of
                                        select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:extent/tei:dimensions/tei:height/@unit"
                                    />
                                </tei:val>
                            </tei:attval>
                        </xsl:otherwise>
                    </xsl:choose>
                    <tei:attval>
                        <tei:att>Couleur</tei:att>
                        <tei:val>
                            <xsl:value-of
                                select="translate(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:objectType/@rend, '_', ' ')"
                            />
                        </tei:val>
                    </tei:attval>
                    <xsl:choose>
                        <xsl:when
                            test="count(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:material) > 1">
                            <xsl:for-each
                                select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:material">
                                <tei:attval>
                                    <tei:att>
                                        <xsl:text>Qualité - </xsl:text>
                                        <xsl:value-of select="position()"/>
                                    </tei:att>
                                    <tei:val>
                                        <xsl:value-of select="translate(@rend, '_', ' ')"/>
                                        <xsl:if test="not(. = '')">
                                            <xsl:text> (</xsl:text>
                                            <xsl:value-of select="."/>
                                            <xsl:text>)</xsl:text>
                                        </xsl:if>
                                    </tei:val>
                                </tei:attval>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:when
                            test="count(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:material) = 1">
                            <tei:attval xml:id="quality">
                                <tei:att>Qualité</tei:att>
                                <tei:val>
                                    <xsl:value-of
                                        select="translate(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:material/@rend, '_', ' ')"/>
                                    <xsl:if
                                        test="not(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:material = '')">
                                        <xsl:text> (</xsl:text>
                                        <xsl:value-of
                                            select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:material"/>
                                        <xsl:text>)</xsl:text>
                                    </xsl:if>
                                </tei:val>
                            </tei:attval>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when
                            test="count(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:watermark) > 1">
                            <xsl:for-each
                                select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:watermark">
                                <tei:attval xml:id="watermark">
                                    <tei:att>
                                        <xsl:value-of
                                            select="upper-case(substring(translate(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:watermark/@rend, '_', ' '), 1, 1))"/>
                                        <xsl:value-of
                                            select="substring(translate(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:watermark/@rend, '_', ' '), 2)"/>
                                        <!--<xsl:text> - </xsl:text>
                                        <xsl:value-of select="position()"/>-->
                                    </tei:att>
                                    <tei:val>
                                        <xsl:choose>
                                            <xsl:when test="not(. = '')">
                                                <xsl:value-of select="."/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:text>Oui</xsl:text>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </tei:val>
                                </tei:attval>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:when
                            test="count(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:watermark) = 1">
                            <tei:attval>
                                <tei:att>
                                    <xsl:value-of
                                        select="upper-case(substring(translate(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:watermark/@rend, '_', ' '), 1, 1))"/>
                                    <xsl:value-of
                                        select="substring(translate(tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:watermark/@rend, '_', ' '), 2)"
                                    />
                                </tei:att>
                                <tei:val>
                                    <xsl:choose>
                                        <xsl:when test="not(. = '')">
                                            <xsl:value-of
                                                select="tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:supportDesc/tei:support/tei:watermark"
                                            />
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:text>Oui</xsl:text>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </tei:val>
                            </tei:attval>
                        </xsl:when>
                    </xsl:choose>
                </tei:group>
                <tei:group xml:id="contribution" title="Contributions" set_title="Contribution">
                    <xsl:for-each select="tei:fileDesc/tei:titleStmt/tei:respStmt">
                        <xsl:sort data-type="number" order="ascending" select="tei:resp/@when"/>
                        <tei:set>
                            <tei:attval>
                                <tei:att>Contributeur</tei:att>
                                <tei:val>
                                    <xsl:value-of select="tei:name"/>
                                </tei:val>
                            </tei:attval>
                            <tei:attval>
                                <tei:att>Date</tei:att>
                                <tei:val>
                                    <xsl:value-of select="substring(tei:resp/@when, 7, 2)"/>
                                    <xsl:text>/</xsl:text>
                                    <xsl:value-of select="substring(tei:resp/@when, 5, 2)"/>
                                    <xsl:text>/</xsl:text>
                                    <xsl:value-of select="substring(tei:resp/@when, 1, 4)"/>
                                </tei:val>
                            </tei:attval>
                            <tei:attval>
                                <tei:att>Nature</tei:att>
                                <tei:val>
                                    <xsl:value-of select="tei:resp"/>
                                </tei:val>
                            </tei:attval>
                        </tei:set>
                    </xsl:for-each>
                </tei:group>
            </tei:metadata>
        </xsl:variable>

        <xsl:call-template name="get_metadata_content">
            <xsl:with-param name="metadata" select="$metadata"/>
        </xsl:call-template>

        <br/>
        <xsl:if test="count(tei:profileDesc/tei:textClass/tei:keywords/tei:term) > 0">
            <div class="letter-badges">
                <span>Mots-clefs&non_breakable_space;:</span>
                <xsl:for-each select="tei:profileDesc/tei:textClass/tei:keywords/tei:term">
                    <xsl:sort lang="fr"/>
                    <span class="badge letter-badge">
                        <xsl:value-of select="."/>
                    </span>
                </xsl:for-each>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="get_metadata_content">
        <xsl:param name="metadata" as="node()"/>
        <div class="accordion accordion-flush" id="accordionFlushExample">
            <xsl:for-each select="$metadata/tei:group">
                <div class="accordion-item metadata-group" id="metadata-{@xml:id}">
                    <h2 class="metadata-group-title accordion-header">
                        <button class="accordion-button collapsed" type="button"
                            data-bs-toggle="collapse" data-bs-target="#flush-{@xml:id}">
                            <xsl:value-of select="@title"/>
                            <!--<xsl:text> </xsl:text>
                            <xsl:if test="@subtitle">
                                <span class="metadata-received-pers-id text-muted fs-5">
                                    <xsl:value-of select="@subtitle"/>
                                </span>
                            </xsl:if> -->
                        </button>
                    </h2>
                    <div id="flush-{@xml:id}" class="accordion-collapse collapse"
                        data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">
                            <ul class="metadata-group-content">
                                <xsl:choose>
                                    <xsl:when test="tei:set">
                                        <xsl:for-each select="tei:set">
                                            <li class="metadata-set-title">
                                                <xsl:value-of select="../@set_title"/>
                                                <xsl:text> - </xsl:text>
                                                <xsl:value-of select="position()"/>
                                                <ul class="metadata-set-content">
                                                  <xsl:call-template name="get_metadata_attval">
                                                  <xsl:with-param name="attval" select="."
                                                  as="node()"/>
                                                  </xsl:call-template>
                                                </ul>
                                            </li>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:call-template name="get_metadata_attval">
                                            <xsl:with-param name="attval" select="." as="node()"/>
                                        </xsl:call-template>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </ul>
                        </div>
                    </div>
                </div>
            </xsl:for-each>
        </div>
    </xsl:template>
    <xsl:template name="get_metadata_attval">
        <xsl:param name="attval" as="node()"/>
        <xsl:for-each select="$attval/tei:attval">
            <li class="metadata-group-attval">
                <span class="metadata-group-attribut">
                    <xsl:value-of select="tei:att"/>
                </span>
                <xsl:text>&non_breakable_space;: </xsl:text>
                <span class="metadata-group-value">
                    <xsl:apply-templates select="tei:val" mode="metadata"/>
                </span>
            </li>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="tei:val" mode="metadata">
        <xsl:apply-templates mode="metadata"/>
    </xsl:template>
    <xsl:template match="tei:title" mode="metadata">
        <i>
            <xsl:apply-templates mode="metadata"/>
        </i>
    </xsl:template>

    <xsl:template match="tei:text" mode="note">
        <xsl:for-each select=".//tei:note[@n]">
            <xsl:sort data-type="number" order="ascending" select="@n"/>
            <div class="note_critique" data-note-id="{@n}">
                <span class="note_id">Note n°<xsl:value-of select="@n"/></span>
                <xsl:if test="@type and not(@type = '')">
                    <span class="letter-badges m-3">
                        <span class="badge letter-badge">
                            <xsl:value-of select="@type"/>
                        </span>
                    </span>
                </xsl:if>
                <div class="letter-date note_content">
                    <xsl:apply-templates mode="#current"/>
                    <xsl:text> [</xsl:text>
                    <xsl:for-each select="tokenize(@resp, ' ')">
                        <xsl:value-of select="substring-after(., '#')"/>
                        <xsl:if test="position() &lt; last()">
                            <xsl:text>,&non_breakable_space;</xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:text>]</xsl:text>
                </div>
            </div>
        </xsl:for-each>
        <!-- Traitement des autres notes (non critiques) -->
        <xsl:for-each select=".//tei:note[not(@n)]">
            <xsl:variable name="num">
                <xsl:number count="." level="any"/>
            </xsl:variable>
            <div class="letter note_critique note_autre d-none " data-note-id="{$num}">
                <a class="btn cp note_id">
                    <xsl:choose>
                        <xsl:when test="@type = 'traduction'">
                            <xsl:text>Traduction</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>Note</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </a>
                <div class="letter-date note_content">
                    <xsl:apply-templates mode="#current"/>
                </div>
            </div>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:ref[@type = 'renvoi']" mode="note">
        <xsl:choose>
            <xsl:when test=". = ''">
                <xsl:apply-templates mode="#current"/>
                <a href="/letter/{substring-after(@target,'cp')}"
                    data-cp-id="{substring-after(@target,'cp')}" target="_blank">
                    <sup class="material-icons md-18">link</sup>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <a href="/letter/{substring-after(@target,'cp')}"
                    data-cp-id="{substring-after(@target,'cp')}" target="_blank">
                    <xsl:apply-templates mode="#current"/>
                </a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:ref[@type = 'reference_externe'] | tei:ref" mode="note">
        <xsl:choose>
            <xsl:when test=". = ''">
                <xsl:apply-templates mode="#current"/>
                <a href="{@target}" target="_blank">
                    <sup class="material-icons  md-18">open_in_new</sup>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <a href="{@target}" target="_blank">
                    <xsl:apply-templates mode="#current"/>
                </a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- Les citations ne sont pas encodées dans les notes
    <xsl:template match="tei:quote[@rend = 'diplo']" mode="note">
        <blockquote class="p-3 quote font-weight-light text-muted fs-5">
            <xsl:apply-templates mode="diplo"/>
        </blockquote>
    </xsl:template>-->
    <xsl:template match="tei:del" mode="note">
        <s>
            <xsl:apply-templates mode="#current"/>
        </s>
    </xsl:template>
    <xsl:template match="tei:add" mode="note"> &lt;<xsl:apply-templates mode="#current"/>&gt; </xsl:template>

    <xsl:template match="tei:listBibl[count(tei:bibl) > 1]" mode="note">
        <ul>
            <xsl:apply-templates mode="li"/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:listBibl" mode="note">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:bibl" mode="note">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:bibl" mode="li">
        <li>
            <xsl:apply-templates mode="note"/>
        </li>
    </xsl:template>
    <xsl:template match="tei:author" mode="note">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <!-- Text transformation (add useful non breakable spaces, transform apostrophes) -->
    <xsl:template match="text()" mode="#all">
        <xsl:variable name="apos">
            <xsl:text>'</xsl:text>
        </xsl:variable>
        <xsl:value-of
            select="replace(replace(replace(., ' ([;:!?»])', '&non_breakable_space;$1'), '([«]) ', '$1&non_breakable_space;'), $apos, 'ʼ')"
        />
    </xsl:template>

    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template name="hard_debug">
        <div class="container border text-center m-auto mb-1 p-1 ">
            <p>Si la liste est vide, vous avez gagné !</p>
            <button class="btn btn-primary" type="button" data-toggle="collapse"
                data-target="#check_results" aria-expanded="false" aria-controls="check_results"
                >Voir les résultats</button>
        </div>
        <xsl:variable name="xml-files" select="collection(concat($xml-dir, '/.?select=*_*.xml'))"/>
        <div class="collapse" id="check_results">
            <xsl:for-each select="$xml-files">
                <div class="container border border-warning">
                    <p>Vérification de la lettre <b><xsl:value-of
                                select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:correspDesc/@xml:id"
                            /></b></p>
                    <ul>
                        <xsl:apply-templates
                            select="//tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date"
                            mode="hard_debug"/>
                        <xsl:apply-templates select="//tei:profileDesc/tei:correspDesc"
                            mode="hard_debug"/>
                        <xsl:apply-templates select="//tei:note[ancestor-or-self::tei:body]"
                            mode="hard_debug"/>
                        <xsl:apply-templates select="//tei:note" mode="hard_debug"/>
                        <xsl:apply-templates select="//tei:pb[not(@facs)]" mode="hard_debug"/>
                    </ul>
                </div>
            </xsl:for-each>
        </div>
        <xsl:call-template name="debug_xslt_from_odd"/>
    </xsl:template>

    <xsl:template
        match="tei:profileDesc/tei:correspDesc/tei:correspAction[@type = 'write']/tei:date"
        mode="hard_debug">
        <xsl:if test="not(@when) and not(@atLeast and @atMost)">
            <li class="text-danger">
                <!--<xsl:text>[</xsl:text>
                <xsl:value-of select="sf:current-mode-name()"/>
                <xsl:text>] </xsl:text>-->
                <xsl:text>Attribut @when (ou bien duo des attributs @atLeast et @atMost) attendu pour le correspAction de @type 'write'.</xsl:text>
                <xmp class="fs-6 text font-monospace">
                    <xsl:copy>
                        <xsl:copy-of select="@*"/>
                    </xsl:copy>
                </xmp>
            </li>
        </xsl:if>
        <xsl:if test="
                @when and (
                (@when = '')
                or (string-length(@when) &lt; 6)
                or (string-length(@when) = 8 and ((xs:integer(substring(@when, 7, 2)) &lt; 1) or (xs:integer(substring(@when, 7, 2)) > 31))))">
            <li class="text-danger">
                <xsl:text>Valeur @when inatendue. Celle-ci doit respecter le format aaaammdd ou bien aaaamm</xsl:text>
                <xmp class="fs-6 text font-monospace">
                    <xsl:copy>
                        <xsl:copy-of select="@*"/>
                    </xsl:copy>
                </xmp>
            </li>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:profileDesc/tei:correspDesc" mode="hard_debug">
        <xsl:if test="not(tei:correspAction[@type = 'write']/tei:persName/@ref)">
            <li class="text-warning">
                <xsl:text>Attribut @ref attendu pour l'expéditeur de la lettre (persName de la correspAction de @type 'write').</xsl:text>
            </li>
        </xsl:if>
        <xsl:if test="not(tei:correspAction[@type = 'received']/tei:persName/@ref)">
            <li class="text-warning">
                <xsl:text>Attribut @ref attendu pour le ou la destinataire de la lettre (persName de la correspAction de @type 'received').</xsl:text>
            </li>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:note[ancestor-or-self::tei:body]" mode="hard_debug">
        <xsl:if test="not(@n)">
            <li>
                <!--<xsl:text>[</xsl:text>
                <xsl:value-of select="sf:current-mode-name()"/>
                <xsl:text>] </xsl:text>-->
                <xsl:text>Attribut @n attendu pour les notes dans body.</xsl:text>
                <xmp class="fs-6 text font-monospace">
                    <xsl:copy>
                        <xsl:copy-of select="@*"/>
                    </xsl:copy>
                </xmp>
            </li>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:note" mode="hard_debug">
        <xsl:if test="not(@resp)">
            <li>
                <!--<xsl:text><xsl:text>[</xsl:text>
                <xsl:value-of select="sf:current-mode-name()"/>
                <xsl:text>] </xsl:text>-->
                <xsl:text>Attribut @resp attendu pour les notes.</xsl:text>
                <xmp class="fs-6 text font-monospace">
                    <xsl:copy-of select="."/>
                </xmp>
            </li>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:pb[not(@facs)]" mode="hard_debug">
        <li>
            <!--<xsl:text>[</xsl:text>
            <xsl:value-of select="sf:current-mode-name()"/>
            <xsl:text>] </xsl:text>-->
            <xsl:text>Attribut @facs attendu pour les pb.</xsl:text>
            <xmp class="fs-6 text font-monospace">
                <xsl:copy-of select="@*"/>
                <xsl:copy-of select="."/>
            </xmp>
        </li>
    </xsl:template>

    <xsl:template match="tei:*" mode="#all">
        <span class="btn btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:value-of select="name(.)"/>
            <xsl:if test="@*">
                <small class="fw-light">
                    <xsl:for-each select="@*">
                        <xsl:text> @</xsl:text>
                        <xsl:value-of select="name()"/>
                        <xsl:text>='</xsl:text>
                        <xsl:value-of select="."/>
                        <xsl:text>'</xsl:text>
                    </xsl:for-each>
                </small>
            </xsl:if>
            <xsl:if test="$debug = 1">
                <sup>
                    <!--<xsl:text> [</xsl:text>
                <xsl:value-of select="sf:current-mode-name()"/>
                <xsl:text>] </xsl:text>-->
                </sup>
            </xsl:if>
        </span>
        <xsl:if test="$debug = 1">
            <xsl:message>
                <!--<xsl:text>[</xsl:text>
                <xsl:value-of select="sf:current-mode-name()"/>
                <xsl:text>] </xsl:text>-->
                <xsl:text>Élement non pris en charge par la XSLT :</xsl:text>
                <xsl:copy/>
                <xsl:copy-of select="@*"/>
            </xsl:message>
        </xsl:if>
    </xsl:template>
    <xsl:template name="debug_xslt_from_odd">
        <h2>Debug XSLT from ODD</h2>
        <p>Pour le moment, vérifie uniquement qu'une règle au moins existe pour les elemntSpec de
            l'ODD et informe des modes concernés.</p>
        <xsl:variable name="odd-file" select="collection('../.?select=*.odd')"/>
        <xsl:variable name="xsl-file" select="collection('.?select=*_askme.xsl')"/>
        <ul>
            <xsl:for-each select="$odd-file//tei:elementSpec">
                <li>
                    <xsl:variable name="match">
                        <xsl:text>tei:</xsl:text>
                        <xsl:value-of select="@ident"/>
                    </xsl:variable>
                    <!--<xsl:value-of select="count($xsl-file//xsl:template[contains(@match,$match)])"/>-->
                    <b>
                        <xsl:value-of select="@ident"/>
                    </b>
                    <xsl:text> </xsl:text>
                    <xsl:for-each-group
                        select="$xsl-file//xsl:template[contains(@match, $match)]/tokenize(@mode, ' ')"
                        group-by=".">
                        <button>
                            <xsl:attribute name="class">
                                <xsl:text>btn btn-sm p-1 m-1 </xsl:text>
                                <xsl:value-of select="."/>
                            </xsl:attribute>
                            <xsl:value-of select="."/>
                        </button>
                    </xsl:for-each-group>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>
