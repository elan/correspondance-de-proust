<?php

namespace App\Command;

use App\Entity\Letter;
use App\Service\LetterManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputOption;

class DeleteLettersCommand extends Command
{
    protected static $defaultName = 'app:delete-letters';
    protected static $defaultDescription = 'delete letters';
    private $em;
    private $lm;

    public function __construct(EntityManagerInterface $em, LetterManager $lm)
    {
        $this->lm = $lm;
        $this->em = $em;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('force', null, InputOption::VALUE_NONE, 'Securize command');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $force = true === $input->getOption('force');

        if($force) {
            $letters = $this->em->getRepository(Letter::class)->findAll();
            foreach ($letters as $letter) {
                $this->lm->delete($letter);
            }
    
            $io->success('Letters deleted');
    
            return Command::SUCCESS;
        }

        $io->error('force flag needed');

        return 0;
    }
}
