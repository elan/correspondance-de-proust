<?php

namespace App\Command;

use App\Service\XMLManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class InvalidateXmlCacheCommand extends Command
{
    protected static $defaultName = 'app:cache:invalidate';
    protected static $defaultDescription = 'xml cache invalidate';
    private $xm;

    public function __construct(XMLManager $xm)
    {
        $this->xm = $xm;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->xm->cacheInvalidate();
        
        $io->success('XML cache invalidated');

        return Command::SUCCESS;
    }
}
