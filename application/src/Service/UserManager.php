<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class UserManager
{
    private $em;
    private $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function setRole(User $user, $roles)
    {
        $user->setRoles($roles);
        $this->saveUser($user);

        return;
    }

    public function getCurrentUser()
    {
        return $this->security->getUser();
    }

    public function saveUser(User $user)
    {
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }
}
