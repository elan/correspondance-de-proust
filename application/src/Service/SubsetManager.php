<?php

namespace App\Service;

use App\Entity\Letter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;

class SubsetManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getBySubset($name, $value = null, $invert = false)
    {
        switch ($name) {
            case 'letters_redated':
                $xpath = "//profileDesc/correspDesc/correspAction[@type='write']/date[@change='".$value."']";
                break;
            case 'letters_entirety':
                $xpath = "//f[@name='entirety'][@fVal='".$value."']";
            case 'letters_signed':
                $xpath = "//signed";
            case 'intended_title':
                $xpath = "//seg[@type = 'titre_envisagé']";
        }   

        $letters = $this->em->getRepository(Letter::class)->findAll();
        $lettersId = [];

        foreach ($letters as $letter) {
            $xmlPath = "/var/www/public/upload/letters/" . $letter->getFilename();
            $crawler = new Crawler();
            $crawler->addXmlContent(file_get_contents($xmlPath));

            $crawler = $crawler->filterXPath($xpath);

            if((!$invert && $crawler->count() > 0) || $invert && $crawler->count() == 0) {
                $lettersId[] = $letter->getProustId();
            }
            unset($crawler);
        }

        return $lettersId;
    }
}
