<?php

namespace App\Service;

use App\Entity\Letter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class LetterManager
{
    private $em;
    private $xmlManager;
    private $flash;
    private $pm;

    public function __construct(EntityManagerInterface $em, XMLManager $xmlManager, PersonManager $pm, FlashBagInterface $flash)
    {
        $this->flash = $flash;
        $this->em = $em;
        $this->xmlManager = $xmlManager;
        $this->pm = $pm;
    }

    public function delete(Letter $letter) 
    {
        // remove html cache
        $this->xmlManager->letterCacheInvalidate($letter);

        // remove xml file
        $xmlPath = "/var/www/public/upload/letters/" . $letter->getFilename();
        unlink($xmlPath);

        // remove db
        $this->em->remove($letter);
        $this->em->flush();

        return;
    }

    public function findByQuery($query, $searchIn, $caseSensitive)
    {
        $results = [];
        $letters = $this->em->getRepository(Letter::class)->findAll();
        $xslPath = "/var/www/public/data/xslt/corr-proust_tei2html_askme.xsl";

        $letterText = in_array("text", $searchIn);
        $notes = in_array("notes", $searchIn);
        $metadata = in_array("metadata", $searchIn);

        $lastChar = mb_substr($query, -1);
        if ($lastChar != "*") {
            // need to match fullwords only
            $query = "\b".$query."\b";
        } else {
            $query = "\b".$query;
            // remove joker
            $query = mb_substr($query, 0, -1);
        }

        $query = "/".$query."/";

        if(!$caseSensitive) {
            $query .= "i";
        }

        foreach ($letters as $letter) {
            $xmlPath = "/var/www/public/upload/letters/" . $letter->getFilename();
            $text = "";

            if ($letterText) {
                $text .= $this->xmlManager->testSaxon($xmlPath, $xslPath, "linear");
                $text .= $this->xmlManager->testSaxon($xmlPath, $xslPath, "diplo");
            }
            
            if ($notes) {
                $text .= $this->xmlManager->testSaxon($xmlPath, $xslPath, "note");
            }

            if ($metadata) {
                $text .= $this->xmlManager->testSaxon($xmlPath, $xslPath, "metadata");
            }

            $text = strip_tags($text);

            $contains = preg_match($query, $text);
            if($contains != false && $contains != null && $contains != 0) {
                $results[] = $letter->getProustId();
            }
        }

        return $results;
    }

    public function getPublishedLetterIds($published = true)
    {
        $letters = $this->em->getRepository(Letter::class)->findByPublished($published);
        $publishedLetterIds = [];
        foreach ($letters as $letter) {
            $publishedLetterIds[] = $letter->getProustId();
        }

        return json_encode($publishedLetterIds);
    }

    public function lettersToJson($xmlPath, $xslPath)
    {
        $json = $this->xmlManager->testSaxon($xmlPath, $xslPath, "json");

        return $json;
    }

    // public function letterToArray(Letter $letter)
    // {
    //     // should be done through XSL ?
    //     // store json for each letter in a db field ?
    //     $letterArray = [];
    //     $letterArray['id'] = $letter->getProustId();
    //     $letterArray['from'] = $letter->getSender()->getName();
    //     $letterArray['to'] = $letter->getRecipient()->getName();
    //     $letterArray['date'] = $letter->getHumanDate();
    //     $letterArray['boundaries'] = [];
    //     $letterArray['boundaries']['lower'] = -2244240000000; // second from 1970 (*1000 for js)
    //     $letterArray['boundaries']['higher'] = -2126044800000;
    //
    //     return $letterArray;
    // }

    public function postUpload($folder, $newFilename, $originalFilename, $isSuperAdmin)
    {
        $canErase = true;
        $crawler = $this->xmlManager->initCrawler($folder . '/' . $newFilename);
        $proustId = $this->xmlManager->extract($crawler, 'correspDesc', 'xml:id');
        $proustId = substr($proustId, 2);

        if ($letter = $this->em->getRepository(Letter::class)->findOneByProustId($proustId)) {
            // admin (and super-admin) can erase unpublished letter.
            if ($letter->getPublished() && !$isSuperAdmin) {
                $canErase = false;
            }
            if ($canErase) {
                $path = $folder . '/' . $letter->getFilename();
                if (file_exists($path)) {
                    unlink($path);
                }
            } else {
                $path = $folder . '/' . $newFilename;
                // remove uploaded letter when not enough permissions
                if (file_exists($path)) {
                    unlink($path);
                }
            }
        } else {
            $letter = new Letter();
            $letter->setProustId($proustId);
            $letter->setCreationDate(new \DateTime());
            $letter->setPublished(false);
        }

        $this->xmlManager->letterCacheInvalidate($letter);

        if ($canErase) {
            $humanDate = $this->xmlManager->extract($crawler, 'correspAction[type="write"] > date');
            $senderName = $this->xmlManager->extract($crawler, 'correspAction[type="write"] > persName', 'ref');
            $recipientName = $this->xmlManager->extract($crawler, 'correspAction[type="received"] > persName', 'ref');

            if ($humanDate === null || $senderName === null || $recipientName === null) {
                unlink($folder . '/' . $newFilename);
                return false;
            }

            $sender = $this->pm->getOrCreate($senderName);
            $recipient = $this->pm->getOrCreate($recipientName);

            $letter->setOriginalFilename($originalFilename);
            $letter->setFilename($newFilename);
            $letter->setUpdateDate(new \DateTime());
            $letter->setSender($sender);
            $letter->setRecipient($recipient);
            $letter->setHumanDate($humanDate);

            $this->em->persist($letter);
            $this->em->flush();

            return $letter;
        }

        $this->flash->add('danger', 'Seuls les super_administrateurs peuvent écraser des lettres déjà publiées.');
        return false;
    }
}
