<?php

namespace App\Controller;

use App\Entity\Letter;
use App\Entity\User;
use App\Form\XmlUploadType;
use App\Service\LetterManager;
use App\Service\UserManager;
use App\Service\XMLManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/preprod-letters", name="admin_preprod_letters")
     * @IsGranted("ROLE_ADMIN")
     */
    public function preprodLetters(HttpClientInterface $client, LetterManager $lm)
    {
        $url = $this->getParameter('preprod');
        $response = $client->request("GET", $url . "api/letters-preprod");
        $json = $response->getContent();
        $letters = json_decode($json);

        $publishedLetters = $lm->getPublishedLetterIds(true);
        $unpublishedLetters = $lm->getPublishedLetterIds(false);

        return $this->render('letter/preprod.html.twig', [
            'letters' => $letters,
            'publishedLetters' => $publishedLetters,
            "unpublishedLetters" => $unpublishedLetters
        ]);
    }

    /**
     * @Route("/preprod-letter/load/{proustId}", name="admin_preprod_letter_load")
     * @IsGranted("ROLE_ADMIN")
     */
    public function preprodLetterLoad($proustId, HttpClientInterface $client, LetterManager $lm, XMLManager $xm)
    {
        $url = $this->getParameter('preprod');
        $response = $client->request("GET", $url . "api/letter-preprod/" . $proustId);
        $content = $response->getContent();
        $array = json_decode($content);
        $filename = $array->filename;
        $newFilename = preg_replace("/-\w+\.xml/", "-" . uniqid() . '.xml', $filename);
        $originalFilename = preg_replace("/-\w+\.xml/", '.xml', $newFilename);
        $folder = $this->getParameter('letters_path');

        $filePath = 'tmp/' . $newFilename;
        $fs = new Filesystem();
        $fs->dumpFile($filePath, $array->content);

        $xslPath = $this->getParameter('xslt_path') . DIRECTORY_SEPARATOR . "corr-proust_tei2html_askme.xsl";
        $testXML = $xm->testOneLetter($filePath, $xslPath);
        if ($testXML !== true) {
            $this->addFlash('danger', $testXML);
            return $this->redirectToRoute('admin_upload');
        }

        // TEST MOVE FILE
        try {
            $fs->rename($filePath, $folder . DIRECTORY_SEPARATOR . $newFilename, true);
        } catch (FileException $e) {
        }

        // UPLOAD POST PROCESSING
        $admin = $this->isGranted('ROLE_ADMIN');
        if ($letter = $lm->postUpload($folder, $newFilename, $originalFilename, $admin)) {
            $this->addFlash('success', 'upload_ok');
            return $this->redirectToRoute('letter', ['proustId' => $letter->getProustId()]);
        }

        return $this->redirectToRoute("admin_preprod_letters");
    }

    /**
     * @Route("/users", name="admin_users")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function users(EntityManagerInterface $em)
    {
        $users = $em->getRepository(User::class)->findBy([], ['roles' => "DESC"]);

        return $this->render('users/index.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/logs", name="admin_logs")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function logs(EntityManagerInterface $em)
    {
        $letters = $em->getRepository(Letter::class)->findBy([], ['updateDate' => "DESC"]);

        return $this->render('letter/logs.html.twig', ['letters' => $letters]);
    }

    /**
     * @Route("/upload/", name="admin_upload")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function upload(Request $request, LetterManager $lm, XMLManager $xm)
    {
        $form = $this->createForm(XmlUploadType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $xmlFile = $form->get('xml')->getData();
            if ($xmlFile) {
                $originalFilename = pathinfo($xmlFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = $originalFilename . '-' . uniqid() . '.' . $xmlFile->guessExtension();
                $folder = $this->getParameter('letters_path');

                // TEST JSON ON SIGNE FILE
                $xslPath = $this->getParameter('xslt_path') . DIRECTORY_SEPARATOR . "corr-proust_tei2html_askme.xsl";
                $testXML = $xm->testOneLetter($xmlFile, $xslPath);
                if ($testXML !== true) {
                    $this->addFlash('danger', $testXML);
                    return $this->redirectToRoute('admin_upload');
                }

                // TEST MOVE FILE
                try {
                    $xmlFile->move($folder, $newFilename);
                } catch (FileException $e) {
                }

                // UPLOAD POST PROCESSING
                $isSuperAdmin = $this->isGranted('ROLE_SUPER_ADMIN');
                if ($letter = $lm->postUpload($folder, $newFilename, $originalFilename, $isSuperAdmin)) {
                    $this->addFlash('success', 'upload_ok');
                    return $this->redirectToRoute('letter', ['proustId' => $letter->getProustId()]);
                } else {
                    return $this->redirectToRoute('admin_upload');
                }
            }
        }

        return $this->renderForm('base/upload.html.twig', [
            'form' => $form,
            'title' => 'upload'
        ]);
    }

    /**
     * @Route("/upload-common/", name="admin_upload_common")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function uploadCommon(Request $request)
    {
        $form = $this->createForm(XmlUploadType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $xmlFile = $form->get('xml')->getData();
            if ($xmlFile) {
                $originalFilename = pathinfo($xmlFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = $originalFilename . '.' . $xmlFile->guessExtension();
                $folder = $this->getParameter('commons_path');
                try {
                    $xmlFile->move($folder, $newFilename);
                } catch (FileException $e) {
                }
            }
            return $this->redirectToRoute('homepage');
        }

        return $this->renderForm('base/upload.html.twig', [
            'form' => $form,
            'title' => 'upload_common'
        ]);
    }

    /**
     * @Route("/upload-edito/", name="admin_upload_edito")
     * @IsGranted("ROLE_ADMIN")
     */
    public function uploadEdito(Request $request)
    {
        $form = $this->createForm(XmlUploadType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $xmlFile = $form->get('xml')->getData();
            if ($xmlFile) {
                $originalFilename = pathinfo($xmlFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = $originalFilename . '.' . $xmlFile->guessExtension();
                $folder = $this->getParameter('edito_path');
                try {
                    $xmlFile->move($folder, $newFilename);
                } catch (FileException $e) {
                }
            }
            return $this->redirectToRoute('homepage');
        }

        return $this->renderForm('base/upload.html.twig', [
            'form' => $form,
            'title' => 'upload_edito'
        ]);
    }

    /**
     * @Route("/set_role/{id}/{role}", name="admin_set_role")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function setRole(User $user, $role, UserManager $um)
    {
        $um->setRole($user, [$role]);

        return $this->redirectToRoute('admin_users');
    }
}
