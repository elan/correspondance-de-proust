<?php

namespace App\Controller;

use App\Entity\Letter;
use App\Entity\PersonCacheUIUC;
use App\Form\TextSearchType;
use App\Service\LetterManager;
use App\Service\PersonCacheManager;
use App\Service\XMLManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class LetterController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function search(Request $request, LetterManager $lm): Response
    {
        $form = $this->createForm(TextSearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $xmlPath = $this->getParameter('letters_path') . DIRECTORY_SEPARATOR . "test.xml";
            $xslPath = $this->getParameter('xslt_path') . DIRECTORY_SEPARATOR . "corr-proust_tei2html_askme.xsl";
            $publishedLetterIds = $lm->getPublishedLetterIds();
            $json = $lm->lettersToJson($xmlPath, $xslPath);

            // On récupère les id des lettres qui correspondent à la requête
            $query = $form->get('query')->getData();
            $searchIn = $form->get('search_in')->getData();
            $caseSensitive = $form->get('case_sensitive')->getData();
            $lettersId = $lm->findByQuery($query, $searchIn, $caseSensitive);

            if(empty($lettersId)) {
                $this->addFlash('warning', 'no_result');
                return $this->render('letter/search.html.twig', [
                    'form' => $form->createView()
                ]);
            }

            // On supprime du json les lettres qui ne font pas partie de la liste précédemment créee.
            $jsonElementsToDel = [];
            $jsonToArray = json_decode($json, true);
            foreach ($jsonToArray as $key => $value) {
                if(!in_array($value["id"], $lettersId)) {
                    $jsonElementsToDel[] = $key;
                }
            }
            foreach ($jsonElementsToDel as $i) {
                unset($jsonToArray[$i]);
            }

            $jsonToArray = array_values($jsonToArray);
            $json = json_encode($jsonToArray);

            return $this->render('letter/search-results.html.twig', [
                'query' => $query,
                'json' => $json,
                'publishedLetterIds' => $publishedLetterIds
            ]);
        }

        return $this->render('letter/search.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/letters", name="letters")
     */
    public function index(EntityManagerInterface $em, LetterManager $lm): Response
    {
        $xmlPath = $this->getParameter('letters_path') . DIRECTORY_SEPARATOR . "test.xml";
        $xslPath = $this->getParameter('xslt_path') . DIRECTORY_SEPARATOR . "corr-proust_tei2html_askme.xsl";
        $json = $lm->lettersToJson($xmlPath, $xslPath);

        $em->getRepository(Letter::class)->findByPublished(true);
        $publishedLetterIds = $lm->getPublishedLetterIds();

        return $this->render('letter/index.html.twig', [
            'publishedLetterIds' => $publishedLetterIds,
            'json' => $json
        ]);
    }

    /**
     * @Route("/random-letter", name="random-letter")
     */
    public function random(EntityManagerInterface $em, LetterManager $lm): Response
    {
        $letters = !$this->isGranted('ROLE_ADMIN')
            ? $em->getRepository(Letter::class)->findByPublished(true)
            : $em->getRepository(Letter::class)->findAll();

        if (count($letters) == 0) {
            $this->addFlash('warning', 'no_letter_published_yet');
            return $this->redirectToRoute('letters');
        }
        shuffle($letters);

        return $this->redirectToRoute('letter', ["proustId" => $letters[0]->getProustId()]);
    }

    /**
     * @Route("/letter/{proustId}", name="letter", options={"expose"=true})
     */
    public function display(?Letter $letter = null, XMLManager $xm, LetterManager $lm): Response
    {
        if (!$letter) {
            $this->addFlash('warning', 'letter_not_uploaded_yet');
            return $this->redirectToRoute('letters');
        }

        if (!$letter->getPublished() && !$this->isGranted('ROLE_ADMIN')) {
            $this->addFlash('warning', 'letter_not_publish_yet');
            return $this->redirectToRoute('letters');
        }

        $xmlPath = $this->getParameter('letters_path') . DIRECTORY_SEPARATOR . $letter->getFilename();
        $xslPath = $this->getParameter('xslt_path') . DIRECTORY_SEPARATOR . "corr-proust_tei2html_askme.xsl";
        $title = $xm->testSaxon($xmlPath, $xslPath, "title");
        $diplo = $xm->testSaxon($xmlPath, $xslPath, "diplo");
        $linear = $xm->testSaxon($xmlPath, $xslPath, "linear");
        $note = $xm->testSaxon($xmlPath, $xslPath, "note");
        $metadata = $xm->testSaxon($xmlPath, $xslPath, "metadata");
        $facs = $xm->testSaxon($xmlPath, $xslPath, "facs");
        $debug = $xm->testSaxon($xmlPath, $xslPath, "debug-info");

        $publishedLetterIds = $lm->getPublishedLetterIds();

        return $this->render('letter/display.html.twig', [
            'letter' => $letter,
            'title' => $title,
            'diplo' => $diplo,
            'linear' => $linear,
            'note' => $note,
            'facs' => $facs,
            'metadata' => $metadata,
            'debug' => $debug,
            'publishedLetterIds' => $publishedLetterIds
        ]);
    }

    /**
     * Retrieve person info from UIUC API
     *
     * @Route("/infos-person/{key}", name="info-person", methods={"GET"}, options={"expose"=true})
     */
    public function getPersonInfoAction($key, PersonCacheManager $pcm, EntityManagerInterface $em)
    {
        if ($cache = $em->getRepository(PersonCacheUIUC::class)->findOneByName($key)) {
            $content = $cache->getContent();
        } else {
            $content = $pcm->getInfo($key);
            if (!$em->getRepository(PersonCacheUIUC::class)->findOneByName($key)) {
                $em->getRepository(PersonCacheUIUC::class)->create($key, $content);
            }
        }

        $json = json_decode($content);

        return $this->render('person/popover.html.twig', [
            'json' => $json,
        ]);
    }

    /**
     * @Route("/letter/publish/{id}/{publish}", name="letter_publish")
     * @IsGranted("ROLE_ADMIN")
     */
    public function publish(Letter $letter, $publish, EntityManagerInterface $em): Response
    {
        $letter->setPublished($publish);
        $em->persist($letter);
        $em->flush();

        return $this->redirectToRoute('letter', ['proustId' => $letter->getProustId()]);
    }


    /**
     * @Route("/letter/invalidate/{id}", name="letter_cache_invalidate")
     * @IsGranted("ROLE_ADMIN")
     */
    public function cacheInvalidate(Letter $letter, XMLManager $xm): Response
    {
        $xm->letterCacheInvalidate($letter);

        return $this->redirectToRoute('letter', ['proustId' => $letter->getProustId()]);
    }


    /**
     * @Route("/letter/publish-with-cp/{proustId}/{publish}", name="letter_publish_cp")
     * @IsGranted("ROLE_ADMIN")
     */
    public function publishWithCP(Letter $letter, $publish, EntityManagerInterface $em): Response
    {
        $letter->setPublished($publish);
        $em->persist($letter);
        $em->flush();

        return $this->redirectToRoute('admin_preprod_letters');
    }

    /**
     * @Route("/letter/delete/{proustId}", name="letter_delete")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function delete(Letter $letter, LetterManager $lm): Response
    {
        $lm->delete($letter);
        $this->addFlash('success', 'delete_ok');

        return $this->redirectToRoute('letters');
    }
}
