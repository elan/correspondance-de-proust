<?php

namespace App\Controller;

use App\Service\XMLManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @method User getUser()
 */
class EditorialController extends AbstractController
{
    private $xm;
    private $xslPath;
    private $xmlPath;

    public function __construct(ContainerBagInterface $params, XMLManager $xm)
    {
        $this->xm = $xm;
        $this->xslPath = $params->get('xslt_path') . DIRECTORY_SEPARATOR . "corr-proust_tei2editorial.xsl";
        $this->xmlPath = $params->get('edito_path') . DIRECTORY_SEPARATOR . "test.xml";
    }


    /**
     * @Route("/actualites", name="actualites")
     */
    public function actualites(): Response
    {
        $rss = simplexml_load_file('https://elan.hypotheses.org/category/corrproust/feed');

        return $this->render('edito/actu.html.twig', ['rss' => $rss]);
    }

    /**
     * @Route("/legal", name="legal")
     */
    public function legal(): Response
    {
        return $this->render('edito/legal.html.twig');
    }

    /**
     * @Route("/presentation/project", name="project")
     */
    public function project(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'project', $request->getLocale());

        return $this->render('edito/presentation/project.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/presentation/editorial", name="edito_politic")
     */
    public function editorial(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'editorial_policy', $request->getLocale());

        return $this->render('edito/presentation/editorial.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/presentation/team", name="team")
     */
    public function team(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'team', $request->getLocale());

        return $this->render('edito/presentation/team.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/presentation/technic", name="technic")
     */
    public function technic(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'methodology', $request->getLocale());

        return $this->render('edito/presentation/technic.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/guide/abbreviations", name="abbreviations")
     */
    public function abbreviations(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'abbreviations', $request->getLocale());

        return $this->render('edito/guide/abbreviations.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/guide/bibliography", name="bibliography")
     */
    public function bibliography(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'bibliography', $request->getLocale());

        return $this->render('edito/guide/bibliography.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/guide/how_to_cite", name="how_to_cite")
     */
    public function cite(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'cite', $request->getLocale());

        return $this->render('edito/guide/cite.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/guide/user-guide", name="user_guide")
     */
    public function userGuide(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'guide', $request->getLocale());

        return $this->render('edito/guide/user-guide.html.twig', ['content' => $content]);
    }

     /**
     * @Route("/guide/encoding-manual", name="encoding_manual")
     */
    public function encodingManual(): Response
    {

        return $this->render('edito/guide/encoding-manual.html.twig');
    }


    

    /**
     * @Route("/partners/consortium", name="consortium")
     */
    public function consortium(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'consortium', $request->getLocale());

        return $this->render('edito/partners/consortium.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/partners/sponsors", name="sponsors")
     */
    public function sponsors(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'sponsors', $request->getLocale());

        return $this->render('edito/partners/sponsors.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/partners/honor", name="honor")
     */
    public function honor(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'honor', $request->getLocale());

        return $this->render('edito/partners/honor.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/partners/thanks", name="thanks")
     */
    public function thanks(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'acknowledgments', $request->getLocale());

        return $this->render('edito/partners/thanks.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/partners/contact", name="contact")
     */
    public function contact(Request $request): Response
    {
        $content = $this->xm->getEditorial($this->xslPath, $this->xmlPath, 'contact', $request->getLocale());

        return $this->render('edito/partners/contact.html.twig', ['content' => $content]);
    }
}
