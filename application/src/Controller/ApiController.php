<?php

namespace App\Controller;

use App\Entity\Letter;
use App\Service\LetterManager;
use App\Service\SubsetManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/letters-preprod", name="api_preprod_letters")
     */
    public function apiLetters(EntityManagerInterface $em, LetterManager $lm): JsonResponse
    {
        $xmlPath = $this->getParameter('letters_path') . DIRECTORY_SEPARATOR . "test.xml";
        $xslPath = $this->getParameter('xslt_path') . DIRECTORY_SEPARATOR . "corr-proust_tei2html_askme.xsl";
        $json = $lm->lettersToJson($xmlPath, $xslPath);

        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/api/letter-preprod/{proustId}", name="api_preprod_letter")
     */
    public function apiLetter(Letter $letter): JsonResponse
    {
        $xmlPath = $this->getParameter('letters_path') . DIRECTORY_SEPARATOR . $letter->getFilename();
        $xmlContent = file_get_contents($xmlPath);

        $array = ["content" => $xmlContent, "filename" => $letter->getFilename()];

        return new JsonResponse($array);
    }

     /**
     * @Route("api/subset/{name}/{val?false}/{invert}", name="api_subset_json")
     */
    public function subset($name, $val = null, $invert = 0, LetterManager $lm, SubsetManager $sm): JsonResponse
    {
        $xmlPath = $this->getParameter('letters_path') . DIRECTORY_SEPARATOR . "test.xml";
        $xslPath = $this->getParameter('xslt_path') . DIRECTORY_SEPARATOR . "corr-proust_tei2html_askme.xsl";
        $json = $lm->lettersToJson($xmlPath, $xslPath);

        $lettersId = $sm->getBySubset($name, $val, $invert);

         // On supprime du json les lettres qui ne font pas partie de la liste précédemment créee.
         $jsonElementsToDel = [];
         $jsonToArray = json_decode($json, true);
         foreach ($jsonToArray as $key => $value) {
             if(!in_array($value["id"], $lettersId)) {
                 $jsonElementsToDel[] = $key;
             }
         }
         foreach ($jsonElementsToDel as $i) {
             unset($jsonToArray[$i]);
         }

         $jsonToArray = array_values($jsonToArray);
         $json = json_encode($jsonToArray);

         return JsonResponse::fromJsonString($json);
    }
}
