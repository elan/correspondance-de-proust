<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\LetterManager;
use App\Service\SubsetManager;

class SubsetController extends AbstractController
{
    /**
     * @Route("/subset/{name}/{val?false}/{invert}", name="subset")
     */
    public function subset($name, $val = null, $invert = 0, LetterManager $lm, SubsetManager $sm): Response
    {
        $xmlPath = $this->getParameter('letters_path') . DIRECTORY_SEPARATOR . "test.xml";
        $xslPath = $this->getParameter('xslt_path') . DIRECTORY_SEPARATOR . "corr-proust_tei2html_askme.xsl";
        $publishedLetterIds = $lm->getPublishedLetterIds();
        $json = $lm->lettersToJson($xmlPath, $xslPath);

        $lettersId = $sm->getBySubset($name, $val, $invert);

         // On supprime du json les lettres qui ne font pas partie de la liste précédemment créee.
         $jsonElementsToDel = [];
         $jsonToArray = json_decode($json, true);
         foreach ($jsonToArray as $key => $value) {
             if(!in_array($value["id"], $lettersId)) {
                 $jsonElementsToDel[] = $key;
             }
         }
         foreach ($jsonElementsToDel as $i) {
             unset($jsonToArray[$i]);
         }

         $jsonToArray = array_values($jsonToArray);
         $json = json_encode($jsonToArray);

         return $this->render('letter/search-results.html.twig', [
             'query' => $name,
             'value' => $val,
             'invert' => $invert,
             'json' => $json,
             'publishedLetterIds' => $publishedLetterIds
         ]);
    }
    
}
