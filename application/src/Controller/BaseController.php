<?php

namespace App\Controller;

use App\Entity\Letter;
use App\Service\LetterManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @method User getUser()
 */
class BaseController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        return $this->render('base/homepage.html.twig');
    }


    /**
     * @Route("/visualisations", name="visualisations")
     * @IsGranted("ROLE_ADMIN")
     */
    public function visualisations(EntityManagerInterface $em, LetterManager $lm): Response
    {
        $xmlPath = $this->getParameter('letters_path') . DIRECTORY_SEPARATOR . "test.xml";
        $xslPath = $this->getParameter('xslt_path') . DIRECTORY_SEPARATOR . "corr-proust_tei2html_askme.xsl";
        $json = $lm->lettersToJson($xmlPath, $xslPath);

        $em->getRepository(Letter::class)->findByPublished(true);
        $publishedLetterIds = $lm->getPublishedLetterIds();
        return $this->render('base/playground.html.twig', [
            'publishedLetterIds' => $publishedLetterIds,
            'json' => $json
        ]);
    }
}
