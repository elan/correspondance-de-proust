import '../styles/letter.scss';
import {
  Modal
} from 'bootstrap';

window.addEventListener('load', function() {
  letterHandler.init()
})

const letterHandler = {
  viewer: Array(),
  init: function() {
    this.createPageDivs()
    this.checkLetterLinks()
    this.countDebugMsg()
    let openseadragon = require('openseadragon')
    letterHandler.viewer["left"] = openseadragon({
      id: "openseadragon-left",
      showNavigator: false,
      showRotationControl: true,
      zoomInButton: 'osd-zoom-in-left',
      zoomOutButton: 'osd-zoom-out-left',
      homeButton: 'osd-home-left',
      fullPageButton: 'osd-full-page-left',
      nextButton: 'osd-next-left',
      previousButton: 'osd-previous-left',
      rotateLeftButton: 'osd-left-left',
      rotateRightButton: 'osd-right-left'
    });
    letterHandler.viewer["right"] = openseadragon({
      id: 'openseadragon-right',
      showNavigator: false,
      showRotationControl: true,
      zoomInButton: 'osd-zoom-in-right',
      zoomOutButton: 'osd-zoom-out-right',
      homeButton: 'osd-home-right',
      fullPageButton: 'osd-full-page-right',
      nextButton: 'osd-next-right',
      previousButton: 'osd-previous-right',
      rotateLeftButton: 'osd-left-right',
      rotateRightButton: 'osd-right-right'
    });
    const thumbnails = document.querySelectorAll('.img-thumbnail');
    [].forEach.call(thumbnails, function(element) {
      element.addEventListener("click", letterHandler.change);
    });
    letterHandler.display("left", thumbnails[0], false)
    letterHandler.display("right", thumbnails[0], false)

    window.onscroll = function() {
      let body = document.querySelector('body');
      if (window.scrollY >= 300) {
        body.classList.add("scrolled");
      } else {
        body.classList.remove("scrolled");
      }
    };

    let noteCalls = document.querySelectorAll('.note-call');
    [].forEach.call(noteCalls, function(noteCall) {
      noteCall.addEventListener("click", letterHandler.noteCall);
    });

    let toggleBtns = document.querySelectorAll('.toggle-mark');
    [].forEach.call(toggleBtns, function(toggleBtn) {
      toggleBtn.addEventListener("click", letterHandler.toggleMark);
    });
  },
  checkLetterLinks: function() {
    const publishedIds = JSON.parse(document.getElementById('letter').dataset.publishedids)
    let links = document.querySelectorAll('a[href^="#cp"]');
    [].forEach.call(links, function(link) {
      let id =  link.href.split("#cp").pop()
      if (!publishedIds.includes(id)) {
        link.setAttribute('href', "javascript: void(0)")
        link.classList.add("disabled")
      } else {
        let url = Routing.generate('letter', {'proustId': id});
        link.setAttribute('href', url)
        link.setAttribute('target', '_blank')
      }
    });
  },
  toggleMark: function(evt) {
    let btn = evt.currentTarget
    let pos = btn.dataset.pos

    document.querySelector('#' + pos + '-panel').classList.toggle('disabled-mark')
  },
  noteCall: function(evt) {
    evt.stopPropagation()
    let noteCall = evt.currentTarget
    let noteId = noteCall.dataset.noteId
    let isOtherNote = noteCall.classList.contains('note-call-other')

    noteId = noteId.substr(noteId.lastIndexOf("n") + 1);
    let selector = '.note_critique[data-note-id="' + noteId + '"] .note_content'
    let content = document.querySelector(selector).innerHTML
    let modalSelector = (isOtherNote) ? 'note-otherModal' : 'noteModal'
    document.querySelector("#"+modalSelector+" .note-content").innerHTML = content;
    document.querySelector("#note-title-id").innerHTML = noteId;

    new Modal(document.getElementById(modalSelector), {}).toggle()
  },
  change: function(evt) {
    let previous
    let thumbnail = evt.currentTarget
    let pos = thumbnail.dataset.pos
    let url = thumbnail.dataset.img

    if (previous = document.querySelector('#' + pos + '-panel .img-thumbnail.current')) {
      previous.classList.remove('current')
    }
    thumbnail.classList.add('current')
    letterHandler.display(pos, thumbnail, true)
  },

  display: function(where, thumbnail, scroll) {
    let credit = thumbnail.dataset.credit
    let url = thumbnail.dataset.img
    let position = thumbnail.id.substr(thumbnail.id.lastIndexOf("-") + 1);
    if (url) {
      letterHandler.viewer[where].open({
        type: 'image',
        url: url
      });
    } 

    document.querySelector('#img-source-'+where).innerHTML = (credit) ? credit : ""
    this.colorPageDiv(position, scroll);
  },
  countDebugMsg: function() {
    const count = document.querySelectorAll('#left-panel .debug-info').length;
    if (count > 0) {
      for (var el of document.querySelectorAll('.debug-related')) {

        el.classList.remove('d-none')
      }
      for (var el of document.querySelectorAll('.bug-count')) {
        el.innerHTML = count
      }
    }

  },
  changeImg: function(evt) {
    let previous
    let target = evt.target
    let div = target.closest('[data-img-id]')
    let id = div.getAttribute('data-img-id')

    if (previous = document.querySelector('.img-thumbnail.current')) {
      previous.classList.remove('current')
    }

    let leftThumbnail = document.querySelector('#image-left-' + id)
    let rightThumbnail = document.querySelector('#image-right-' + id)

    letterHandler.display("left", leftThumbnail, false)
    letterHandler.display("right", rightThumbnail, false)

    if (previous = document.querySelector('#left-panel .img-thumbnail.current')) {
      previous.classList.remove('current')
    }
    if (previous = document.querySelector('#right-panel .img-thumbnail.current')) {
      previous.classList.remove('current')
    }
    leftThumbnail.classList.add('current')
    rightThumbnail.classList.add('current')
  },
  removePageDivColor: function() {
    let pageTextDivs = document.querySelectorAll('.current-page-text')
    for (var pageTextDiv of pageTextDivs) {
      pageTextDiv.classList.remove('current-page-text')
    }
  },

  colorPageDiv: function(position, scroll) {
    this.removePageDivColor();
    let pageTextDivs = document.querySelectorAll(`.page-div[data-img-id="${position}"]`)
    for (var pageTextDiv of pageTextDivs) {
      pageTextDiv.classList.add('current-page-text')
    }
    if (scroll) {
      let currentPageDiv = document.querySelector('.tab-pane.show .page-div.current-page-text')
      if (currentPageDiv) {
        currentPageDiv.scrollIntoView(true)
      }
    }
  },

  createPageDivs: function() {
    const hrs = document.querySelectorAll('.transcription-pane hr:not(.trait-proustien)')

    for (var hr of hrs) {
      let next = hr.nextSibling
      let div = document.createElement('div')
      hr.parentNode.insertBefore(div, next)
      div.classList.add('page-div')
      div.setAttribute('data-img-id', hr.getAttribute('data-img-id'))
      div.addEventListener("click", letterHandler.changeImg);
      while (next) {
        let node = next
        next = next.nextSibling
        div.appendChild(node)
        if (!next || (next.nodeName == 'HR' && !next.classList.contains('trait-proustien'))) break
      }
    }
  }
}
