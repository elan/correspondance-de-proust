import '../styles/playground.scss';
import 'tom-select/dist/css/tom-select.bootstrap5.min.css';
import cytoscape from 'cytoscape';
import Chart from 'chart.js/auto';

const {
  createApp
} = Vue

const app = createApp({
  compilerOptions: {
    delimiters: ["${", "}"]
  },
  data() {
    return {
      cy: null,
      cy2: null,
      letters: [],
      personsInventaire: [],
      dataInventaire: null,
      persons: [],
      tags: [],
      dateFrom: 1890,
      dateTo: 1922,
      minLettersTotal: 1,
      minLettersByYear: 1,
      maxLettersByYear: 100,
      letterSender: "all",
      personsLetterCount: {},
      dataChartjs: null,
      people: [],
      checkedPeople: [],
      searchText: ""
    }
  },

  mounted() {
    //this.letters = JSON.parse(document.getElementById('app').dataset.json);
    let dataInventaire = []

    fetch("json/inventaire.json")
      .then(response => response.json())
      .then(json => {
        json.forEach((item, i) => {
          let from = item["Expéditeur"]
          let date = item["Date(Kolb)"]
          let to = item["Destinataire"]
          let id = item.id
          if (from != "" && to != "" && /^\d{4}$/.test(date)) {
            dataInventaire.push(item)
          }
        });
        app.dataInventaire = dataInventaire
        this.initBubbleAll();
        // this.initCytoscape();
      })


    // this.initBarChart();
    // this.initBubbles();
    // this.initBubbles2();
  },
  computed:{
    matchingPpl: function(){
      return this.people.filter(function(p){
        return app.searchText == '' || p.toLowerCase().includes(app.searchText.toLowerCase()) || app.checkedPeople.includes(p)
      }).sort()
    },
  },
  methods: {
    changeFilters() {
      this.personsLetterCount = {}
      document.querySelector("#myChart4").remove()
      const canvas = document.createElement("canvas");
      canvas.setAttribute('id', "myChart4")
      document.querySelector("#chart4-container").appendChild(canvas)
      app.initBubbleAll()
    },
    getPersonsFromIndex(index) {
      return this.persons[value]
    },
    initBubbleAll() {
      let dataInventaire = []
      app.personsLetterCount = {}
      // console.log(json)
      app.dataInventaire.forEach((letter, i) => {
        let from = letter["Expéditeur"]
        let date = letter["Date(Kolb)"]
        let to = letter["Destinataire"]
        let id = letter.id

        if (date >= app.dateFrom && date <= app.dateTo) {
          let isFromMarcel = true
          let isToMarcel = true
          let ppl
          const marcel = ["Marcel Proust", "Proust, Marcel", "Marcel, Proust"]

          if (!this.personsInventaire.includes(from) && !marcel.includes(from)) {
            this.personsInventaire.push(from)
          }

          if (!marcel.includes(from)) {
            ppl = from
            isFromMarcel = false
          }

          if (!this.personsInventaire.includes(to) && !marcel.includes(to)) {
            this.personsInventaire.push(to)
          }

          if (!marcel.includes(to)) {
            isToMarcel = false
            ppl = to
          }

          // Stockage des noms de personnes
          if (!app.people.includes(to)  && !marcel.includes(to)) {
            app.people.push(to)
          }
          if (!app.people.includes(from) && !marcel.includes(to)) {
            app.people.push(from)
          }

          if (app.letterSender == "all" || (app.letterSender == "from_proust" && isFromMarcel) || (app.letterSender == "to_proust" && isToMarcel)) {
            if (app.personsLetterCount.hasOwnProperty(ppl)) {
              app.personsLetterCount[ppl] += 1
            } else {
              app.personsLetterCount[ppl] = 1
            }
            // let pplIndex = this.personsInventaire.indexOf(ppl)
            // test si déjà lettre pour une mm person et même timestamp
            let exchange = dataInventaire.find((xchange) => xchange.x == date && xchange.y == ppl)
            if (exchange) {
              exchange.r += 1
              exchange.id++
            } else {
              dataInventaire.push({
                id: 1,
                x: date, // timestamp
                y: ppl, // person index
                r: 1 // volume
              })
            }

          }
        }
      }); // end foreach

      // filter data with byYear & total threshold, and assign numeric value to Y (persons index)
      let finalPersons = []
      let finalInventaire = []
      dataInventaire.forEach((data, i) => {
        if (data.id >= app.minLettersByYear && data.id <= app.maxLettersByYear && app.personsLetterCount[data.y] >= app.minLettersTotal && (app.checkedPeople.length == 0 || app.checkedPeople.includes(data.y))) {
          if (finalPersons.indexOf(data.y) == -1) {
            finalPersons.push(data.y)
          }
          data.y = finalPersons.indexOf(data.y)
          finalInventaire.push(data)
        }
      })

      // Rescale bubble radius
      let max = Math.max(...finalInventaire.map(o => o.id))
      let min = Math.min(...finalInventaire.map(o => o.id))
      let empan = max - min
      let maxNewScale = 35
      finalInventaire.forEach((item, i) => {
        item.r = Math.round(item.r / empan * maxNewScale + 1)
      });

      // app.dataChartjs = finalInventaire

      let nbPersons = finalPersons.length
      // -( maxSortie - minSortie / maxEntrée ) * Entrée + maxSortie
      // todo -> moins linéaire > rester proche de 0.1 plus longtemps
      let aspectRatio = -(0.9 / 530) * nbPersons + 1

      const data = {
        datasets: [{
          label: 'Par personne & par date',
          data: finalInventaire,
          backgroundColor: '#927c26'
        }]
      };
      const config = {
        type: 'bubble',
        data: data,
        options: {
          aspectRatio: aspectRatio,
          // responsive: true,
          // maintainAspectRatio: false,
          plugins: {
            tooltip: {
              callbacks: {
                label: function(context) {
                  let label = finalPersons[context.parsed.y] + " - " + context.parsed.x + " (" + context.raw.id + " lettres)";
                  return label;
                }
              }
            }
          },
          scales: {
            y: {
              display: true,
              ticks: {
                autoSkip: true,
                stepSize: 1,
                callback: function(value, index, values) {
                  return finalPersons[value]
                }
              }
            },
            x: {
              ticks: {
                autoSkip: false,
                stepSize: 1,
                type: 'time',

                // stepSize: 31536000,
                // callback: function(value, index, values) {
                //   const date = new Date(value);
                //
                //   return date.toLocaleDateString();
                // }
              }

            }
          }
        }

      };
      const myChart = new Chart(
        document.getElementById('myChart4'),
        config
      );
    },
    initBubbles2() {
      let bubblesData = this.generateBubblesData2();
      let tags = this.tags
      let persons = this.persons
      let data = {
        datasets: [{
          label: 'Par tag & par date',
          data: bubblesData,
          backgroundColor: '#927c26'
        }]
      };
      const config = {
        type: 'bubble',
        data: data,
        options: {
          aspectRatio: 2,
          plugins: {
            tooltip: {
              callbacks: {
                label: function(context) {
                  let date = new Date(context.parsed.x);
                  let label = persons[context.parsed.y] + " - " + date.toLocaleDateString() + " (" + context.raw.id + ")";
                  return label;
                }
              }
            }
          },
          scales: {
            y: {
              ticks: {
                autoSkip: false,
                stepSize: 1,
                callback: function(value, index, values) {
                  return tags[value]
                }
              }
            },
            x: {
              ticks: {
                type: 'time',
                count: 24,
                callback: function(value, index, values) {
                  const date = new Date(value);
                  return date.toLocaleDateString();
                }
              }

            }
          }
        }
      };

      const myChart = new Chart(
        document.getElementById('myChart3'),
        config
      );
    },
    initBarChart() {
      let labels = this.generateBarChartLabels();
      labels.sort()
      const data = {
        labels: labels,
        datasets: [{
            label: 'Reçu',
            data: this.generateBarChartData(labels, "to"),
            backgroundColor: "#927c26"
          },
          {
            label: 'Envoyé',
            data: this.generateBarChartData(labels, "from"),
            backgroundColor: "#005d95"
          }
        ]
      };
      const config = {
        type: 'bar',
        data: data,
        options: {
          scales: {
            y: {
              beginAtZero: true,
              ticks: {
                stepSize: 1
              }
            }
          }
        },
      };
      const myChart = new Chart(
        document.getElementById('myChart'),
        config
      );
    },
    generateBubblesData2() {
      let data = []
      this.letters.forEach((letter, i) => {
        let tags = letter.tags
        let date = letter.boundaries.lower
        let id = letter.id

        tags.forEach((tag, i) => {
          if (!this.tags.includes(tag)) {
            this.tags.push(tag)
          }
          let tagIndex = this.tags.indexOf(tag)
          let exchange = data.find((xchange) => xchange.x == date && xchange.y == tagIndex)

          if (exchange) {
            exchange["r"] += 4
            exchange.id += " - " + id
          } else {
            data.push({
              id: id,
              x: date,
              y: tagIndex,
              r: 4
            })
          }
        });
      });

      // console.log(this.tags)
      // console.log(data.filter((xchange) => xchange.y == null).length);
      // const unique = [...new Set(data.map(item => item.r))];
      // console.log(unique)


      return data;
    },
    generateBarChartLabels() {
      let labels = [];
      const letters = this.letters
      letters.forEach((letter, i) => {
        let from = letter.from_canonical
        let to = letter.to_canonical
        const marcel = "Proust, Marcel"
        if (!labels.includes(from) && from != marcel) {
          labels.push(from)
        }
        if (!labels.includes(to) && to != marcel) {
          labels.push(to)
        }
      })

      return labels
    },
    generateBarChartData(labels, direction) {
      const letters = this.letters
      let data = []
      labels.forEach((label, i) => {
        let count = letters.filter((letter) => letter[direction == "from" ? "from_canonical" : "to_canonical"] === label).length;
        data.push(count)
      })

      return data
    },
    generateCytoscapeData() {
      let elements = [];
      const letters = app.dataInventaire
      let startWeight = 0.10
      let incrementWeight = 0.10
      letters.forEach((letter, i) => {

        let from = letter["Expéditeur"]
        let date = letter["Date(Kolb)"]
        let to = letter["Destinataire"]
        let id = letter.id

        // from node
        let fromEl = elements.find(el => el.data.id == from)
        if (!fromEl) {
          elements.push({
            data: {
              id: from,
              weight: startWeight,
              label: from
            }
          })
        } else {
          fromEl.data.weight += (fromEl.data.weight < 100) ? incrementWeight : 0
        }

        // to node
        let toEl = elements.find(el => el.data.id == to)
        if (!toEl) {
          elements.push({
            data: {
              id: to,
              weight: startWeight,
              label: to
            }
          })
        } else {
          toEl.data.weight += (toEl.data.weight < 100) ? incrementWeight : 0
        }

        let edgeId = from + "__" + to
        let edge = elements.find(el => el.data.id == edgeId)
        if (!edge) {
          elements.push({
            data: {
              id: edgeId,
              target: to,
              source: from,
              weight: startWeight
            }
          })
        } else {
          edge.data.weight += (edge.data.weight < 100) ? incrementWeight : 0
        }
      });


      return elements.filter(function(el) {
        if (el.data.weight > 3) {
          return el
        }
      })
    },
    initCytoscape() {
      let elements = this.generateCytoscapeData()
      this.cy = cytoscape({
        container: document.getElementById('cy'),
        elements: elements,
        style: [{
            selector: 'node',
            style: {
              'color': 'black',
              'background-color': '#005d95',
              // "text-valign": "center",
              // "text-halign": "center",
              'label': 'data(label)',
              'width': 'data(weight)',
              'height': 'data(weight)'
            }
          },
          {
            selector: 'edge',
            style: {
              'width': 'data(weight)',
              'line-color': '#927c26',
              'target-arrow-color': '#927c26',
              'target-arrow-shape': 'triangle',
              'curve-style': 'bezier'
            }
          }
        ],
      });
      let options = {
        name: 'cose',
        ready: function() {},
        stop: function() {},
        animate: true,
        animationEasing: undefined,
        animationDuration: undefined,
        animateFilter: function(node, i) {
          return false;
        },
        animationThreshold: 250,
        // Number of iterations between consecutive screen positions update
        refresh: 500,
        // Whether to fit the network view after when done
        fit: true,
        // Padding on fit
        padding: 0,
        // Constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
        boundingBox: undefined,
        // Excludes the label when calculating node bounding boxes for the layout algorithm
        nodeDimensionsIncludeLabels: false,
        // Randomize the initial positions of the nodes (true) or use existing positions (false)
        randomize: true,
        // Extra spacing between components in non-compound graphs
        componentSpacing: 200,
        // Node repulsion (non overlapping) multiplier
        nodeRepulsion: function(node) {
          return 4000000;
        },
        // Node repulsion (overlapping) multiplier
        nodeOverlap: 300,
        // Ideal edge (non nested) length
        idealEdgeLength: function(edge) {
          return 32;
        },
        // Divisor to compute edge forces
        edgeElasticity: function(edge) {
          return 10;
        },
        // Nesting factor (multiplier) to compute ideal edge length for nested edges
        nestingFactor: 3,
        // Gravity force (constant)
        gravity: 1,
        // Maximum number of iterations to perform
        numIter: 2000,
        // Initial temperature (maximum node displacement)
        initialTemp: 1000,
        // Cooling factor (how the temperature is reduced between consecutive iterations
        coolingFactor: 0.99,
        // Lower temperature threshold (below this point the layout will end)
        minTemp: 1.0
      };
      let layout = this.cy.layout(options);
      layout.run();
      this.cy.fit()
      this.cy.center()
    },
  }

}).mount('#app')
